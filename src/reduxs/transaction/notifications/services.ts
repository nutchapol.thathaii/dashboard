import axios from 'axios'
import { endpoint } from './constants'

export const fetchGetNotificationTransactionAll = () => axios.get(endpoint.getNotificationTransactionAll)
