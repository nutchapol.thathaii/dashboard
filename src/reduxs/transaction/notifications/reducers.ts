import { getType } from 'typesafe-actions'
import { RootAction } from 'typings/reduxs/Actions'
import { initialState } from './constants'
import actions from './actions'

const notificationReducer = (state: INotificationAllState = initialState, action: RootAction):
INotificationAllState  => {
  switch (action.type) {
    case getType(actions.getNotificationTransactionAllAction):
      return {
        ...state,
        isFetching: true,
      }
    case getType(actions.getNotificationTransactionAllSuccessAction):
      return {
        isFetching: false,
        data: action.payload.data.data,
        code: action.payload.data.code,
      }

    case getType(actions.getNotificationTransactionAllFailureAction):
      return {
        isFetching: false,
        error: action.payload.message,
        code: action.payload.code,
      }
    case getType(actions.updateNotificationTransactionAllAction):
      return {
        isFetching: true,
        ...state,
      }
    case getType(actions.updateNotificationTransactionAllActionSuccessAction):
      return {
        isFetching: false,
        data: action.payload.data,
      }
    case getType(actions.updateNotificationTransactionAllActionFailureAction):
      return {
        ...state,
      }
    case getType(actions.clearNotificationTransactionAll):
      return initialState
    default:
      return state
  }
}

export default notificationReducer