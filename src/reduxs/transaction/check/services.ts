import axios from 'axios'
import { endpoint } from './constants'

export const fetchPutTransaction = (data: ITransactionPutData) => axios.put(endpoint.putTransaction, data)