import { Epic } from 'redux-observable'
import { from, of } from 'rxjs'
import {
  catchError,
  exhaustMap,
  takeUntil,
  filter,
  map,
} from 'rxjs/operators'
import { isActionOf } from 'typesafe-actions'
import { RootAction } from 'typings/reduxs/Actions'
import { fetchPutTransaction } from './services'
import actions from './actions'

const putEpic: Epic<RootAction, RootAction, RootReducers> = (action$, store, dependencies) =>
  action$.pipe(
    filter(isActionOf(actions.putTransactionAction)),
    exhaustMap(action =>
      from(fetchPutTransaction(action.payload))
        .pipe(
          map(actions.putTransactionSuccessAction),
          catchError(error => of(actions.putTransactionFailureAction(error))),
          takeUntil(action$.pipe(filter(isActionOf(actions.putTransactionCancelAction))))
        ),
    )
  )

export default [
  putEpic,
]
