import { getType } from 'typesafe-actions'
import { RootAction } from 'typings/reduxs/Actions'
import { initialState } from './constants'
import actions from './actions'

const putReducer = (state: ITransactionPutState = initialState, action: RootAction): ITransactionPutState => {
  switch (action.type) {
    case getType(actions.putTransactionAction):
      return {
        ...state,
        isFetching: true,
      }
    case getType(actions.putTransactionSuccessAction):
      return {
        isFetching: false,
        data: action.payload.data.data,
        code: action.payload.data.code,
      }

    case getType(actions.putTransactionFailureAction):
      return {
        isFetching: false,
        error: action.payload.message,
        code: action.payload.code,
      }
    default:
      return state
  }
}

export default putReducer