import { getType } from 'typesafe-actions'
import { RootAction } from 'typings/reduxs/Actions'
import { initialState, initialStateLotterNumberSession, initialStateDepositWithdraw } from './constants'
import { combineReducers } from 'redux'
import actions from './actions'

const reportOverviewReducer = (state: IReportOverviewReduxState = initialState, action: RootAction)
    : IReportOverviewReduxState => {
    switch (action.type) {
        case getType(actions.getReportOverviewAction):
            return {
                ...state,
                isFetching: true,
            }
        case getType(actions.getReportOverviewSuccessAction):
            return {
                isFetching: false,
                data: action.payload.data.data,
                code: action.payload.data.code,
            }

        case getType(actions.getReportOverviewFailureAction):
            return {
                isFetching: false,
                error: action.payload.message,
                code: action.payload.code,
            }
        default:
            return state
    }
}

const reportYegeeReducer = (state: IReportYegeeReduxState = initialState, action: RootAction)
    : IReportYegeeReduxState => {
    switch (action.type) {
        case getType(actions.getReportYegeeAction):
            return {
                ...state,
                isFetching: true,
            }
        case getType(actions.getReportYegeeSuccessAction):
            return {
                isFetching: false,
                data: action.payload.data.data,
                code: action.payload.data.code,
            }

        case getType(actions.getReportYegeeFailureAction):
            return {
                isFetching: false,
                error: action.payload.message,
                code: action.payload.code,
            }
        default:
            return state
    }
}

const reportLotterNumberSessionReducer = (
    state: IReportLotterNumberSessionReduxState = initialStateLotterNumberSession,
    action: RootAction)
    : IReportLotterNumberSessionReduxState => {
    switch (action.type) {
        case getType(actions.getReportLotterNumberSessionAction):
            return {
                isFetching: true,
            }
        case getType(actions.getReportLotterNumberSessionSuccessAction):
            return {
                isFetching: false,
                data: action.payload.data.data,
                code: action.payload.data.code,
            }

        case getType(actions.getReportLotterNumberSessionFailureAction):
            return {
                isFetching: false,
                error: action.payload.message,
                code: action.payload.code,
            }
        default:
            return state
    }
}

const reportLotterDepositWithdrawReducer = (
    state: IReportDepositWithdrawReduxState = initialStateDepositWithdraw,
    action: RootAction)
    : IReportDepositWithdrawReduxState => {
    switch (action.type) {
        case getType(actions.postReportDepositWithdrawAction):
            return {
                isFetching: true,
            }
        case getType(actions.postReportDepositWithdrawSuccessAction):
            return {
                isFetching: false,
                data: action.payload.data.data,
                code: action.payload.data.code,
            }

        case getType(actions.postReportDepositWithdrawFailureAction):
            return {
                isFetching: false,
                error: action.payload.message,
                code: action.payload.code,
            }
        default:
            return state
    }
}

export default combineReducers({
    overview: reportOverviewReducer,
    yegee: reportYegeeReducer,
    lotterNumberSession: reportLotterNumberSessionReducer,
    lotterDepositWithdraw:reportLotterDepositWithdrawReducer,
})