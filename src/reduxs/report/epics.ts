import { Epic } from 'redux-observable'
import { from, of } from 'rxjs'
import {
    catchError,
    exhaustMap,
    takeUntil,
    filter,
    map,
} from 'rxjs/operators'
import { isActionOf } from 'typesafe-actions'
import { RootAction } from 'typings/reduxs/Actions'
import {
    fetchReportOverview,
    fetchReportYegee,
    fetchReportLotterNumberSession,
    fetchpostReportDepositWithdraw,
} from './services'
import actions from './actions'

const reportOverviewEpic: Epic<RootAction, RootAction, RootReducers> = (action$, store, dependencies) =>
    action$.pipe(
        filter(isActionOf(actions.getReportOverviewAction)),
        exhaustMap(action =>
            from(fetchReportOverview(action.payload))
                .pipe(
                    map(actions.getReportOverviewSuccessAction),
                    catchError(error => of(actions.getReportOverviewFailureAction(error))),
                    takeUntil(action$.pipe(filter(isActionOf(actions.getReportOverviewCancelAction))))
                ),
        )
    )

const reportYegeeEpic: Epic<RootAction, RootAction, RootReducers> = (action$, store, dependencies) =>
    action$.pipe(
        filter(isActionOf(actions.getReportYegeeAction)),
        exhaustMap(action =>
            from(fetchReportYegee(action.payload))
                .pipe(
                    map(actions.getReportYegeeSuccessAction),
                    catchError(error => of(actions.getReportYegeeFailureAction(error))),
                    takeUntil(action$.pipe(filter(isActionOf(actions.getReportYegeeCancelAction))))
                ),
        )
    )

const reportLotterNumberSessionEpic: Epic<RootAction, RootAction, RootReducers> = (action$, store, dependencies) =>
    action$.pipe(
        filter(isActionOf(actions.getReportLotterNumberSessionAction)),
        exhaustMap(action =>
            from(fetchReportLotterNumberSession(action.payload))
                .pipe(
                    map(actions.getReportLotterNumberSessionSuccessAction),
                    catchError(error => of(actions.getReportLotterNumberSessionFailureAction(error))),
                    takeUntil(action$.pipe(filter(isActionOf(actions.getReportLotterNumberSessionCancelAction))))
                ),
        )
    )

const reportDepositWithdrawEpic: Epic<RootAction, RootAction, RootReducers> = (action$, store, dependencies) =>
    action$.pipe(
        filter(isActionOf(actions.postReportDepositWithdrawAction)),
        exhaustMap(action =>
            from(fetchpostReportDepositWithdraw(action.payload))
                .pipe(
                    map(actions.postReportDepositWithdrawSuccessAction),
                    catchError(error => of(actions.postReportDepositWithdrawFailureAction(error))),
                    takeUntil(action$.pipe(filter(isActionOf(actions.postReportDepositWithdrawCancelAction))))
                ),
        )
    )

export default [
    reportOverviewEpic,
    reportYegeeEpic,
    reportLotterNumberSessionEpic,
    reportDepositWithdrawEpic,
]
