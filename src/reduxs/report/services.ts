import axios from 'axios'
import { endpoint } from './constants'

export const fetchReportOverview = (data: IRequestReportOverview) => axios.post(`${endpoint.getReportOverview}`, data)
export const fetchReportYegee = (data: IRequestReportYegee) => axios.post(`${endpoint.getReportYegee}`, data)
export const fetchReportLotterNumberSession = (data: IRequestReportLotterNumberSession) => axios.post(`${endpoint.getReportLotterNumberSession}`, data)
export const fetchpostReportDepositWithdraw = (data: IRequestReportDepositWithdraw) => axios.post(`${endpoint.postReportDepositWithdraw}`, data)