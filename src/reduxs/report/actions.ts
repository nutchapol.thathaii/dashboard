import { createAction } from 'typesafe-actions'
import {
    GET_REPORT_OVERVIEW_REQUEST,
    GET_REPORT_OVERVIEW_CANCEL,
    GET_REPORT_OVERVIEW_SUCCESS,
    GET_REPORT_OVERVIEW_FAILURE,

    GET_REPORT_YEGEE_REQUEST,
    GET_REPORT_YEGEE_CANCEL,
    GET_REPORT_YEGEE_SUCCESS,
    GET_REPORT_YEGEE_FAILURE,


    GET_REPORT_LOTTER_NUMBER_SESSION_REQUEST,
    GET_REPORT_LOTTER_NUMBER_SESSION_CANCEL,
    GET_REPORT_LOTTER_NUMBER_SESSION_SUCCESS,
    GET_REPORT_LOTTER_NUMBER_SESSION_FAILURE,

    POST_REPORT_DEPOSIT_WITHDRAW_REQUEST,
    POST_REPORT_DEPOSIT_WITHDRAW_CANCEL,
    POST_REPORT_DEPOSIT_WITHDRAW_SUCCESS,
    POST_REPORT_DEPOSIT_WITHDRAW_FAILURE,
} from './constants'

import { AxiosResponse, AxiosError } from 'axios'
import { Serie } from '@nivo/line'

const getReportOverviewAction = createAction(GET_REPORT_OVERVIEW_REQUEST,
    reslove => (data: IRequestReportOverview) => reslove(data)
)

const getReportOverviewSuccessAction = createAction(
    GET_REPORT_OVERVIEW_SUCCESS,
    resolve => (data: AxiosResponse<APIResponse<IReportOverview<Serie>>>) => resolve(data))

const getReportOverviewFailureAction = createAction(
    GET_REPORT_OVERVIEW_FAILURE,
    resolve => (error: AxiosError) => resolve(error))

const getReportOverviewCancelAction = createAction(GET_REPORT_OVERVIEW_CANCEL)

const getReportYegeeAction = createAction(GET_REPORT_YEGEE_REQUEST,
    reslove => (data: IRequestReportYegee) => reslove(data)
)

const getReportYegeeSuccessAction = createAction(
    GET_REPORT_YEGEE_SUCCESS,
    resolve => (data: AxiosResponse<APIResponse<IReportYegee>>) => resolve(data))

const getReportYegeeFailureAction = createAction(
    GET_REPORT_YEGEE_FAILURE,
    resolve => (error: AxiosError) => resolve(error))

const getReportYegeeCancelAction = createAction(GET_REPORT_YEGEE_CANCEL)

const getReportLotterNumberSessionAction = createAction(GET_REPORT_LOTTER_NUMBER_SESSION_REQUEST,
    reslove => (data: IRequestReportLotterNumberSession) => reslove(data)
)

const getReportLotterNumberSessionSuccessAction = createAction(
    GET_REPORT_LOTTER_NUMBER_SESSION_SUCCESS,
    resolve => (data: AxiosResponse<APIResponse<IReportLotterNumberSession>>) => resolve(data))

const getReportLotterNumberSessionFailureAction = createAction(
    GET_REPORT_LOTTER_NUMBER_SESSION_FAILURE,
    resolve => (error: AxiosError) => resolve(error))

const getReportLotterNumberSessionCancelAction = createAction(GET_REPORT_LOTTER_NUMBER_SESSION_CANCEL)

const postReportDepositWithdrawAction = createAction(POST_REPORT_DEPOSIT_WITHDRAW_REQUEST,
    reslove => (data: IRequestReportDepositWithdraw) => reslove(data)
)

const postReportDepositWithdrawSuccessAction = createAction(
    POST_REPORT_DEPOSIT_WITHDRAW_SUCCESS,
    resolve => (data: AxiosResponse<APIResponse<IReportDepositWithdraw>>) => resolve(data))

const postReportDepositWithdrawFailureAction = createAction(
    POST_REPORT_DEPOSIT_WITHDRAW_FAILURE,
    resolve => (error: AxiosError) => resolve(error))

const postReportDepositWithdrawCancelAction = createAction(POST_REPORT_DEPOSIT_WITHDRAW_CANCEL)

export default {
    getReportOverviewAction,
    getReportOverviewSuccessAction,
    getReportOverviewFailureAction,
    getReportOverviewCancelAction,

    getReportYegeeAction,
    getReportYegeeSuccessAction,
    getReportYegeeFailureAction,
    getReportYegeeCancelAction,


    getReportLotterNumberSessionAction,
    getReportLotterNumberSessionSuccessAction,
    getReportLotterNumberSessionFailureAction,
    getReportLotterNumberSessionCancelAction,

    postReportDepositWithdrawAction,
    postReportDepositWithdrawSuccessAction,
    postReportDepositWithdrawFailureAction,
    postReportDepositWithdrawCancelAction,
}