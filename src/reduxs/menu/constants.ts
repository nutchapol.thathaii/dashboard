export const ON_CHANGE_MENU_REQUEST = 'ON_CHANGE_MENU_REQUEST'
export const ON_CHANGE_MENU_SHOW = 'ON_CHANGE_MENU_SHOW'
export const CLEAR_MENU = 'CLEAR_MENU'

export const initialState: IMenuActiveControl = {
  menu: '/',
}
