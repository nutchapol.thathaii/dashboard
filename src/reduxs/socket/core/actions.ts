import { createAction } from 'typesafe-actions'
import {
  CONNECT_SOCKET,
  CONNECTED_SOCKET,
  CONNECT_SOCKET_ERROR,
  DISCONNECT_SOCKET,
  DISCONNECTED_SOCKET,
  CONECT_FINANCE_DEPOSIT_SOCKET,
  CONECT_FINANCE_WITHDRAW_SOCKET,
} from './constants'

const connectSocketAction = createAction(CONNECT_SOCKET)

const connectedSocketAction = createAction(CONNECTED_SOCKET)

const connectSocketErrorAction = createAction(
  CONNECT_SOCKET_ERROR,
  resolve => (error: any) => resolve(error))

const connectFinanceDepositSocketAction = createAction(CONECT_FINANCE_DEPOSIT_SOCKET,
  reslove => (data:IGetTransactionParams) => reslove(data)
  )
const connectFinanceWithDrawSocketAction = createAction(CONECT_FINANCE_WITHDRAW_SOCKET,
    reslove => (data:IGetTransactionParams) => reslove(data)
  )

const disconnectSocketAction = createAction(DISCONNECT_SOCKET)

const disconnectedSocketAction = createAction(DISCONNECTED_SOCKET)


export default {
  connectSocketAction,
  connectedSocketAction,
  connectSocketErrorAction,
  disconnectSocketAction,
  disconnectedSocketAction,
  connectFinanceDepositSocketAction,
  connectFinanceWithDrawSocketAction,
}