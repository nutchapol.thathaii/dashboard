import webbanksActions from './webbanks/actions'
import rateActions from './rate/actions'
export default {
  ...webbanksActions,
  ...rateActions,
}
