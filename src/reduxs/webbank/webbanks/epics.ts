import { Epic } from 'redux-observable'
import { from, of } from 'rxjs'
import {
  catchError,
  exhaustMap,
  takeUntil,
  filter,
  map,
} from 'rxjs/operators'
import { isActionOf } from 'typesafe-actions'
import { RootAction } from 'typings/reduxs/Actions'
import {
  fetchGetAccountUser,
  fetchGetWebBankActive,
  fetchPutAccountUser,
  fetchDeleteAccountUser,
  fetchPostAccountUser
} from './services'
import actions from './actions'

const webBankEpic: Epic<RootAction, RootAction, RootReducers> = (action$, store, dependencies) =>
  action$.pipe(
    filter(isActionOf(actions.getWebbankAction)),
    exhaustMap(action =>
      from(fetchGetAccountUser(action.payload))
        .pipe(
          map(actions.getWebbankSuccessAction),
          catchError(error => of(actions.getWebbankFailureAction(error))),
          takeUntil(action$.pipe(filter(isActionOf(actions.getWebbankCancelAction))))
        ),
    )
  )

const webBankActiveEpic: Epic<RootAction, RootAction, RootReducers> = (action$, store, dependencies) =>
  action$.pipe(
    filter(isActionOf(actions.getWebbankActiveAction)),
    exhaustMap(_ =>
      from(fetchGetWebBankActive())
        .pipe(
          map(actions.getWebbankActiveSuccessAction),
          catchError(error => of(actions.getWebbankActiveFailureAction(error))),
          takeUntil(action$.pipe(filter(isActionOf(actions.getWebbankActiveCancelAction))))
        ),
    )
  )

const webBankPutEpic: Epic<RootAction, RootAction, RootReducers> = (action$, store, dependencies) =>
  action$.pipe(
    filter(isActionOf(actions.putWebbankAction)),
    exhaustMap(action =>
      from(fetchPutAccountUser(action.payload))
        .pipe(
          map(actions.putWebbankSuccessAction),
          catchError(error => of(actions.getWebbankFailureAction(error))),
          takeUntil(action$.pipe(filter(isActionOf(actions.getWebbankCancelAction))))
        ),
    )
  )

const webBankDeleteEpic: Epic<RootAction, RootAction, RootReducers> = (action$, store, dependencies) =>
  action$.pipe(
    filter(isActionOf(actions.deleteWebbankAction)),
    exhaustMap(action =>
      from(fetchDeleteAccountUser(action.payload))
        .pipe(
          map(actions.deleteWebbankSuccessAction),
          catchError(error => of(actions.deleteWebbankFailureAction(error))),
          takeUntil(action$.pipe(filter(isActionOf(actions.deleteWebbankCancelAction))))
        ),
    )
  )

const webBankPostEpic: Epic<RootAction, RootAction, RootReducers> = (action$, store, dependencies) =>
  action$.pipe(
    filter(isActionOf(actions.postWebbankAction)),
    exhaustMap(action =>
      from(fetchPostAccountUser(action.payload))
        .pipe(
          map(actions.postWebbankSuccessAction),
          catchError(error => of(actions.postWebbankFailureAction(error))),
          takeUntil(action$.pipe(filter(isActionOf(actions.postWebbankCancelAction))))
        ),
    )
  )

const updateWebbankEpic: Epic<RootAction, RootAction, RootReducers> = (action$, store) =>
  action$.pipe(
    filter(isActionOf(actions.updateWebbankAction)),
    map((action) => actions.updateWebbankSuccessAction(action.payload))
  )
export default [
  webBankEpic,
  webBankActiveEpic,
  webBankPutEpic,
  webBankDeleteEpic,
  webBankPostEpic,
  updateWebbankEpic,
]
