import axios from 'axios'
import { endpoint } from './constants'
import { isUndefined, get } from 'lodash'

export const fetchPostAccountUser = (data: IPostWebbankStoreState) => axios.post(`${endpoint.postWebbank}`, data)
export const fetchGetAccountUser = (data: IGetWebBankParams) => {
    const queryParams = new URLSearchParams({
        status: isUndefined(data.queryStatus) ? '' : data.queryStatus,
        page: String(get(data,'page',0) + 1),
        limit: String(get(data,'limit','10')),
        search: get(data,'search',''),
    })
    return axios.get(endpoint.getWebbank, {
        params: queryParams,
    })
}
export const fetchGetWebBankActive = () => axios.get(`${endpoint.getWebbank}?status=ACTIVE`)
export const fetchPutAccountUser = (data: IPutWebbankStoreState) => axios.put(`${endpoint.putWebbank}`, data)
export const fetchDeleteAccountUser = (data: IDeleteWebbankStoreState) =>
axios.delete(`${endpoint.deleteWebbank}`,{ data: data })
