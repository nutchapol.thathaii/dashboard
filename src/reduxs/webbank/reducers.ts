import { combineReducers } from 'redux'
import webbanks from './webbanks/reducers'
import rate from './rate/reducers'
export default combineReducers({
  webbanks,
  rate,
})