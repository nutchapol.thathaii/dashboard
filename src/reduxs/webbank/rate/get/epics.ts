import { Epic } from 'redux-observable'
import { from, of } from 'rxjs'
import {
  catchError,
  exhaustMap,
  takeUntil,
  filter,
  map,
} from 'rxjs/operators'
import { isActionOf } from 'typesafe-actions'
import { fetchGetWebbankRate } from './services'
import { RootAction } from 'typings/reduxs/Actions'
import actions from './actions'

const getBetRateEpic: Epic<RootAction, RootAction, RootReducers> = (action$, store) =>
  action$.pipe(
    filter(isActionOf(actions.getWebbankRateAction)),
    exhaustMap(_ =>
      from(fetchGetWebbankRate())
        .pipe(
          map(actions.getWebbankRateSuccessAction),
          catchError(error => of(actions.getWebbankRateFailureAction(error))),
          takeUntil(action$.pipe(filter(isActionOf(actions.getBetRateCancelAction))))
        ),
    )
  )

export default [
  getBetRateEpic,
]
