import axios from 'axios'
import { endpoint } from './constants'

export const fetchGetWebbankRate = () => axios.get(endpoint.getRateWebbank)