import { getType } from 'typesafe-actions'
import { RootAction } from 'typings/reduxs/Actions'
import { initialState } from './constants'
import actions from './actions'

const getWebbankRateReducer = (
  state: ReducerState<IRate> = initialState,
  action: RootAction
): ReducerState<IRate> => {
  switch (action.type) {
    case getType(actions.getWebbankRateAction):
      return {
        ...state,
        isFetching: true,
      }
    case getType(actions.getWebbankRateSuccessAction):
      return {
        ...state,
        isFetching: false,
        data: action.payload.data.data,
        code: action.payload.status,
      }

    case getType(actions.getWebbankRateFailureAction):
      return {
        ...state,
      }
    default:
      return state
  }
}

export default getWebbankRateReducer