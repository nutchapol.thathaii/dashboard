import webbanks from './webbanks/epics'
import rate from './rate/epics'
export default [
  ...webbanks,
  ...rate,
]
