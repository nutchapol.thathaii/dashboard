import axios from 'axios'
import { endpoint } from './constants'

export const fetchGetWebConfig = () => axios.get(endpoint.getWebConfig)
export const fetchPutWebConfig = (data: IWebConfigPutData) => axios.put(`${endpoint.putWebConfig}`, data)
