import loaderAction from './loader/actions'
import authAction from './auth/actions'
import userAction from './user/actions'
import transactionAction from './transaction/actions'
import betRatesAction from './settings/actions'
import webBankAction from './webbank/actions'
import onChangeAction from './menu/actions'
import newsAction from './news/actions'
import webConfigAction from './webConfig/actions'
import socketAction from './socket/actions'
import affilateAction from './affilate/actions'
import config from './config/actions'
import lotto from './lotto/actions'
import check from './check/actions'
import report from './report/actions'
import credit from './credit/actions'

export default {
  ...socketAction,
  ...userAction,
  ...loaderAction,
  ...authAction,
  ...transactionAction,
  ...betRatesAction,
  ...webBankAction,
  ...onChangeAction,
  ...newsAction,
  ...webConfigAction,
  ...affilateAction,
  ...config,
  ...lotto,
  ...check,
  ...report,
  ...credit,
}