import { combineReducers } from 'redux'
import rate from './rate/reducers'

export default combineReducers({
  rate,
})