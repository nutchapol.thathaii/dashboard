import { combineReducers } from 'redux'
import bet from './bet/reducers'
export default combineReducers({
  bet,
})