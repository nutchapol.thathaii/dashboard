import axios from 'axios'
import { endpoint } from './constants'

export const fetchGetAffilate = () => axios.get(endpoint.getAffilate)
export const fetchPutAffilate = (data: IAffilatePutData) => axios.put(`${endpoint.putAffilate}`, data)
