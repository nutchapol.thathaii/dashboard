import { combineReducers } from 'redux'
import lotto from './lotto/reducers'
import lottoRate from './lottoRate/reducers'
export default combineReducers({
  lotto,
  lottoRate,
})