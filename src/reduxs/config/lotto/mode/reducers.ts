import { getType } from 'typesafe-actions'
import { RootAction } from 'typings/reduxs/Actions'
import { initialState } from './constants'
import actions from './actions'

const lottoModeReducer =
  (state: ReducerState<ILottoSchedule[]> = initialState, action: RootAction): ReducerState<ILottoSchedule[]> => {
    switch (action.type) {
      case getType(actions.putLottoModeAction):
        return {
          ...state,
          isFetching: true,
        }
      case getType(actions.putLottoModeSuccessAction):
        return {
          isFetching: false,
          data: action.payload.data.data,
          code: action.payload.status,
        }

      case getType(actions.putLottoModeFailureAction):
        return {
          isFetching: false,
          error: action.payload.response?.data.devMessage,
          code: action.payload.response?.status,
        }
      default:
        return state
    }
  }

export default lottoModeReducer