import axios from 'axios'
import { endpoint } from './constants'

export const fetchPutLottoMode = (data: ILottoModePutData) => axios.put(endpoint.putLottoMode,data)