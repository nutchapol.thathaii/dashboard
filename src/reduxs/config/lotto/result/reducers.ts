import { getType } from 'typesafe-actions'
import { RootAction } from 'typings/reduxs/Actions'
import { initialState } from './constants'
import actions from './actions'

const lottoResultReducer =
  (state: ReducerState<ILottoResult[]> = initialState, action: RootAction): ReducerState<ILottoResult[]> => {
    switch (action.type) {
      case getType(actions.postLottoResultAction):
        return {
          ...state,
          isFetching: true,
        }
      case getType(actions.postLottoResultSuccessAction):
        return {
          isFetching: false,
          data: action.payload.data.data,
          code: action.payload.status,
        }

      case getType(actions.postLottoResultFailureAction):
        return {
          isFetching: false,
          error: action.payload.response?.data.devMessage,
          code: action.payload.response?.status,
        }
      default:
        return state
    }
  }

export default lottoResultReducer