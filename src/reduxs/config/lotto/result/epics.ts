import { Epic } from 'redux-observable'
import { from, of } from 'rxjs'
import {
  catchError,
  exhaustMap,
  takeUntil,
  filter,
  map,
} from 'rxjs/operators'
import { isActionOf } from 'typesafe-actions'
import { RootAction } from 'typings/reduxs/Actions'
import { fetchPostLottoResult } from './services'
import actions from './actions'

const getLottoScheduleEpic: Epic<RootAction, RootAction, RootReducers> = (action$, store) =>
  action$.pipe(
    filter(isActionOf(actions.postLottoResultAction)),
    exhaustMap(action =>
      from(fetchPostLottoResult(action.payload))
        .pipe(
          map(actions.postLottoResultSuccessAction),
          catchError(error => of(actions.postLottoResultFailureAction(error))),
          takeUntil(action$.pipe(filter(isActionOf(actions.postLottoResultCancelAction))))
        ),
    )
  )

export default [
  getLottoScheduleEpic,
]
