import axios from 'axios'
import { endpoint } from './constants'

export const fetchPostLottoResult = (data: ILottoResultPostData) => axios.post(endpoint.postLottoResult,data)