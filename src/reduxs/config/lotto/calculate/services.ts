import axios from 'axios'
import { endpoint } from './constants'

export const fetchPostLottoCalculate = (data: ILottoCalculatePostData) => axios.post(endpoint.postLottoCalculate,(data))