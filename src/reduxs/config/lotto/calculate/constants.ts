import project from 'constants/project'

export const POST_LOTTO_CALCULATE_REQUEST = 'POST_LOTTO_CALCULATE_REQUEST'
export const POST_LOTTO_CALCULATE_SUCCESS = 'POST_LOTTO_CALCULATE_SUCCESS'
export const POST_LOTTO_CALCULATE_FAILURE = 'POST_LOTTO_CALCULATE_FAILURE'
export const POST_LOTTO_CALCULATE_CANCEL = 'POST_LOTTO_CALCULATE_CANCEL'

export const initialState: ILottoCalculateState = {
  isFetching: false,
  code: 0,
  data: [],
  error: '',
}

export const endpoint = {
  postLottoCalculate: `${project.environment[project.environmentName].api}/lotter/calculate`,
}