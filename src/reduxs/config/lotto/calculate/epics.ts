import { Epic } from 'redux-observable'
import { from, of } from 'rxjs'
import {
  catchError,
  exhaustMap,
  takeUntil,
  filter,
  map,
} from 'rxjs/operators'
import { isActionOf } from 'typesafe-actions'
import { RootAction } from 'typings/reduxs/Actions'
import { fetchPostLottoCalculate } from './services'
import actions from './actions'

const getLottoScheduleEpic: Epic<RootAction, RootAction, RootReducers> = (action$, store) =>
  action$.pipe(
    filter(isActionOf(actions.postLottoCalculateAction)),
    exhaustMap(action =>
      from(fetchPostLottoCalculate(action.payload))
        .pipe(
          map(actions.postLottoCalculateSuccessAction),
          catchError(error => of(actions.postLottoCalculateFailureAction(error))),
          takeUntil(action$.pipe(filter(isActionOf(actions.postLottoCalculateCancelAction))))
        ),
    )
  )

export default [
  getLottoScheduleEpic,
]
