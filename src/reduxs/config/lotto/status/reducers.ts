import { getType } from 'typesafe-actions'
import { RootAction } from 'typings/reduxs/Actions'
import { initialState } from './constants'
import actions from './actions'

const lottoStatusReducer =
  (state: ReducerState<ILottoSchedule[]> = initialState, action: RootAction): ReducerState<ILottoSchedule[]> => {
    switch (action.type) {
      case getType(actions.putLottoStatusAction):
        return {
          ...state,
          isFetching: true,
        }
      case getType(actions.putLottoStatusSuccessAction):
        return {
          isFetching: false,
          data: action.payload.data.data,
          code: action.payload.data.code,
        }

      case getType(actions.putLottoStatusFailureAction):
        return {
          isFetching: false,
          error: action.payload.response?.data.devMessage,
          code: action.payload.response?.status,
        }
      default:
        return state
    }
  }

export default lottoStatusReducer