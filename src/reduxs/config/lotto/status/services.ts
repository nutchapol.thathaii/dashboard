import axios from 'axios'
import { endpoint } from './constants'

export const fetchPutLottoStatus = (data: ILottoStatusPutData) => axios.put(endpoint.putLottoStatus,data)