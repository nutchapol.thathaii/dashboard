import { Epic } from 'redux-observable'
import { from, of } from 'rxjs'
import {
  catchError,
  exhaustMap,
  takeUntil,
  filter,
  map,
} from 'rxjs/operators'
import { isActionOf } from 'typesafe-actions'
import { RootAction } from 'typings/reduxs/Actions'
import { fetchPostLottoRefund } from './services'
import actions from './actions'

const getLottoScheduleEpic: Epic<RootAction, RootAction, RootReducers> = (action$, store) =>
  action$.pipe(
    filter(isActionOf(actions.postLottoRefundAction)),
    exhaustMap(action =>
      from(fetchPostLottoRefund(action.payload))
        .pipe(
          map(actions.postLottoRefundSuccessAction),
          catchError(error => of(actions.postLottoRefundFailureAction(error))),
          takeUntil(action$.pipe(filter(isActionOf(actions.postLottoRefundCancelAction))))
        ),
    )
  )

export default [
  getLottoScheduleEpic,
]
