import { getType } from 'typesafe-actions'
import { RootAction } from 'typings/reduxs/Actions'
import { initialState } from './constants'
import actions from './actions'

const lottoRefundReducer =(state: ILottoRefundState = initialState, action: RootAction): ILottoRefundState => {
    switch (action.type) {
      case getType(actions.postLottoRefundAction):
        return {
          ...state,
          isFetching: true,
        }
      case getType(actions.postLottoRefundSuccessAction):
        return {
          isFetching: false,
          data: action.payload.data.data,
          code: action.payload.status,
        }

      case getType(actions.postLottoRefundFailureAction):
        return {
          isFetching: false,
          error: action.payload.response?.data.devMessage,
          code: action.payload.response?.status,
        }
      default:
        return state
    }
  }

export default lottoRefundReducer