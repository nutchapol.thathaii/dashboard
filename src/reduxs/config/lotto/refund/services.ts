import axios from 'axios'
import { endpoint } from './constants'

export const fetchPostLottoRefund = (data: ILottoRefundPostData) => axios.post(endpoint.postLottoRefund,data)