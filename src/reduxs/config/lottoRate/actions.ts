import list from './list/actions'
import create from './create/actions'
import remove from './remove/actions'
export default {
  ...list,
  ...create,
  ...remove,
}