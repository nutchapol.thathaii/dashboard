import project from 'constants/project'

export const GET_LOTTO_RATE_LEVEL_REQUEST = 'GET_LOTTO_RATE_LEVEL_REQUEST'
export const GET_LOTTO_RATE_LEVEL_SUCCESS = 'GET_LOTTO_RATE_LEVEL_SUCCESS'
export const GET_LOTTO_RATE_LEVEL_FAILURE = 'GET_LOTTO_RATE_LEVEL_FAILURE'
export const GET_LOTTO_RATE_LEVEL_CANCEL = 'GET_LOTTO_RATE_LEVEL_CANCEL'

export const initialState: ReducerState<ILottoRateLevel[]> = {
  isFetching: false,
  code: 0,
  data: [],
  error: '',
}

export const endpoint = {
  getAllLottoRate: `${project.environment[project.environmentName].api}/lotter/rate-level`,
}