import { Epic } from 'redux-observable'
import { from, of } from 'rxjs'
import {
  catchError,
  exhaustMap,
  takeUntil,
  filter,
  map,
} from 'rxjs/operators'
import { isActionOf } from 'typesafe-actions'
import { fetchGetLottoRateLevel } from './services'
import actions from './actions'
import { RootAction } from 'typings/reduxs/Actions'

const getLottoRateLevelEpic: Epic<RootAction, RootAction, RootReducers> = (action$, store) =>
  action$.pipe(
    filter(isActionOf(actions.getLottoRateLevelAction)),
    exhaustMap(action =>
      from(fetchGetLottoRateLevel(action.payload))
        .pipe(
          map(actions.getLottoRateLevelSuccessAction),
          catchError(error => of(actions.getLottoRateLevelFailureAction(error))),
          takeUntil(action$.pipe(filter(isActionOf(actions.getLottoRateLevelCancelAction))))
        ),
    )
  )

export default [
  getLottoRateLevelEpic,
]
