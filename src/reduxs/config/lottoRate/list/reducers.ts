import { getType } from 'typesafe-actions'
import { RootAction } from 'typings/reduxs/Actions'
import { initialState } from './constants'
import actions from './actions'

const getLottoRateLevelReducer
= (state: ReducerState<ILottoRateLevel[]> = initialState,action: RootAction): ReducerState<ILottoRateLevel[]> => {
  switch (action.type) {
    case getType(actions.getLottoRateLevelAction):
      return {
        ...state,
        isFetching: true,
      }
    case getType(actions.getLottoRateLevelSuccessAction):
      return {
        isFetching: false,
        data: action.payload.data.data,
        code: action.payload.status,
      }

    case getType(actions.getLottoRateLevelFailureAction):
      return {
        isFetching: false,
        error: action.payload.message,
        code: action.payload.response?.data.code,
      }
    default:
      return state
  }
}

export default getLottoRateLevelReducer