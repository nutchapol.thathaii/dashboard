import { combineReducers } from 'redux'
import list from './list/reducers'
import create from './create/reducers'
import remove from './remove/reducers'
export default combineReducers({
  list,
  create,
  remove,
})
