import axios from 'axios'
import { endpoint } from './constants'

export const fetchPostLottoRateLevel = (
    data: ILottoCreateRateLevelData) => axios.post(endpoint.postLottoRateLevel,(data)
)