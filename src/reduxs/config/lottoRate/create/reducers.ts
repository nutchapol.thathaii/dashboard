import { getType } from 'typesafe-actions'
import { RootAction } from 'typings/reduxs/Actions'
import { initialState } from './constants'
import actions from './actions'

const lottoResultReducer =
  (state: ReducerState<ILottoRateLevel[]> = initialState, action: RootAction): ReducerState<ILottoRateLevel[]> => {
    switch (action.type) {
      case getType(actions.postLottoRateLevelAction):
        return {
          ...state,
          isFetching: true,
        }
      case getType(actions.postLottoRateLevelSuccessAction):
        return {
          isFetching: false,
          data: action.payload.data.data,
          code: action.payload.data.code,
        }

      case getType(actions.postLottoRateLevelFailureAction):
        return {
          isFetching: false,
          error: action.payload.response?.data.devMessage,
          code: action.payload.response?.status,
        }
      default:
        return state
    }
  }

export default lottoResultReducer