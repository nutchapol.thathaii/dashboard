import { Epic } from 'redux-observable'
import { from, of } from 'rxjs'
import {
  catchError,
  exhaustMap,
  takeUntil,
  filter,
  map,
} from 'rxjs/operators'
import { isActionOf } from 'typesafe-actions'
import { RootAction } from 'typings/reduxs/Actions'
import { fetchPostLottoRateLevel } from './services'
import actions from './actions'

const getLottoScheduleEpic: Epic<RootAction, RootAction, RootReducers> = (action$, store) =>
  action$.pipe(
    filter(isActionOf(actions.postLottoRateLevelAction)),
    exhaustMap(action =>
      from(fetchPostLottoRateLevel(action.payload))
        .pipe(
          map(actions.postLottoRateLevelSuccessAction),
          catchError(error => of(actions.postLottoRateLevelFailureAction(error))),
          takeUntil(action$.pipe(filter(isActionOf(actions.postLottoRateLevelCancelAction))))
        ),
    )
  )

export default [
  getLottoScheduleEpic,
]
