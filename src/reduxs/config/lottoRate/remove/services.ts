import axios from 'axios'
import { endpoint } from './constants'

export const fetchDeleteLottoRateLevel = (
    data: ILottoDeleteRateLevelData) => axios.delete(endpoint.deleteLottoRateLevel,({data: data})
)