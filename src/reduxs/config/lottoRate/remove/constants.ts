import project from 'constants/project'

export const DELETE_LOTTO_RATE_LEVEL_REQUEST = 'DELETE_LOTTO_RATE_LEVEL_REQUEST'
export const DELETE_LOTTO_RATE_LEVEL_SUCCESS = 'DELETE_LOTTO_RATE_LEVEL_SUCCESS'
export const DELETE_LOTTO_RATE_LEVEL_FAILURE = 'DELETE_LOTTO_RATE_LEVEL_FAILURE'
export const DELETE_LOTTO_RATE_LEVEL_CANCEL = 'DELETE_LOTTO_RATE_LEVEL_CANCEL'

export const initialState: ReducerState<ILottoRateLevel[]> = {
  isFetching: false,
  code: 0,
  data: [],
  error: '',
}

export const endpoint = {
  deleteLottoRateLevel: `${project.environment[project.environmentName].api}/lotter/rate-level`,
}