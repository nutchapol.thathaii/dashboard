import { Epic } from 'redux-observable'
import { from, of } from 'rxjs'
import {
  catchError,
  exhaustMap,
  takeUntil,
  filter,
  map,
} from 'rxjs/operators'
import { isActionOf } from 'typesafe-actions'
import { RootAction } from 'typings/reduxs/Actions'
import { fetchDeleteLottoRateLevel } from './services'
import actions from './actions'

const getLottoScheduleEpic: Epic<RootAction, RootAction, RootReducers> = (action$, store) =>
  action$.pipe(
    filter(isActionOf(actions.deleteLottoRateLevelAction)),
    exhaustMap(action =>
      from(fetchDeleteLottoRateLevel(action.payload))
        .pipe(
          map(actions.deleteLottoRateLevelSuccessAction),
          catchError(error => of(actions.deleteLottoRateLevelFailureAction(error))),
          takeUntil(action$.pipe(filter(isActionOf(actions.deleteLottoRateLevelCancelAction))))
        ),
    )
  )

export default [
  getLottoScheduleEpic,
]
