import list from './list/epics'
import create from './create/epics'
import remove from './remove/epics'
export default [
  ...list,
  ...create,
  ...remove,
]
