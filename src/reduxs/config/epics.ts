import lotto from './lotto/epics'
import lottoRate from './lottoRate/epics'
export default [
  ...lotto,
  ...lottoRate,
]
