import lottoActions from './lotto/actions'
import lottoRateActions from './lottoRate/actions'

export default {
  ...lottoActions,
  ...lottoRateActions,
}