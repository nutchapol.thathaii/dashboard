import axios from 'axios'
import { endpoint } from './constants'

export const fetchPutUser = (data: IManagementPutData) => axios.put(`${endpoint.putUser}`, data)