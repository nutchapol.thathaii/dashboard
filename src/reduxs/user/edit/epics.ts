import { Epic } from 'redux-observable'
import { from, of } from 'rxjs'
import {
  catchError,
  exhaustMap,
  filter,
  map,
} from 'rxjs/operators'
import { isActionOf } from 'typesafe-actions'
import {
  fetchPutUser,
} from './services'
import actions from './actions'
import { RootAction } from 'typings/reduxs/Actions'

const userPutEpic: Epic<RootAction, RootAction, RootReducers> = (action$, store, dependencies) =>
  action$.pipe(
    filter(isActionOf(actions.putUserAction)),
    exhaustMap(action =>
      from(fetchPutUser(action.payload))
        .pipe(
          map(actions.putUserSuccessAction),
          catchError(error => of(actions.putUserFailureAction(error))),
        ),
    )
  )
export default [
  userPutEpic,
]