import axios from 'axios'
import { endpoint } from './constants'

export const fetchGetUserMe = () => axios.get(`${endpoint.getUserMe}`)
