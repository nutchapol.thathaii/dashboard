import axios from 'axios'
import { endpoint } from './constants'

export const fetchDeleteUser = (data: IManagementDeleteData) => axios.delete(`${endpoint.deleteUser}`, { data: data })