import axios from 'axios'
import { endpoint } from './constants'

export const fetchPostUser = (data: IManagementPostData) => axios.post(`${endpoint.postUser}`, data)