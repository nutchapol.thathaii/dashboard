import { Epic } from 'redux-observable'
import { from, of } from 'rxjs'
import {
  catchError,
  exhaustMap,
  filter,
  map,
} from 'rxjs/operators'
import { isActionOf } from 'typesafe-actions'
import { fetchGetUserAll } from './services'
import actions from './actions'
import { RootAction } from 'typings/reduxs/Actions'

const userAllEpic: Epic<RootAction, RootAction, RootReducers> = (action$, store, dependencies) =>
  action$.pipe(
    filter(isActionOf(actions.getUserAllAction)),
    exhaustMap(action =>
      from(fetchGetUserAll(action.payload))
        .pipe(
          map(actions.getUserAllSuccessAction),
          catchError(error => of(actions.getUserAllFailureAction(error))),
        ),
    )
  )
export default [
  userAllEpic,
]
