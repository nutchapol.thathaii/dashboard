import tokenAction from './token/actions'
import userMeAction from './me/actions'
import userAllAction from './all/actions'
import userCreateAction from './create/actions'
import userEditAction from './edit/actions'
import userDeleteAction from './delete/actions'

export default {
    ...tokenAction,
    ...userMeAction,
    ...userAllAction,
    ...userCreateAction,
    ...userEditAction,
    ...userDeleteAction,
}