import axios from 'axios'
import { endpoint } from './constants'

export const fetchPostNews = (data: INewsPostData) => axios.post(`${endpoint.postNews}`, data)
export const fetchGetNews = () => axios.get(endpoint.getNews)
export const fetchPutNews = (data: INewsPutData) => axios.put(`${endpoint.putNews}`, data)
export const fetchDeleteNews = (data: INewsDeleteData) => axios.delete(`${endpoint.deleteNews}`, { data: data })
