import { getType } from 'typesafe-actions'
import { RootAction } from 'typings/reduxs/Actions'
import { initialState } from './constants'
import { get } from 'lodash'
import actions from './actions'

const loginReducer = (state: ILoginState = initialState, action: RootAction): ILoginState => {
  switch (action.type) {
    case getType(actions.loginAction):
      return {
        ...state,
        isFetching: true,
      }
    case getType(actions.loginSuccessAction):
      return {
        isFetching: false,
        data: {
          token: get(action.payload.data.data, 'token', ''),
        },
        code: action.payload.data.code,
      }

    case getType(actions.loginFailureAction):
      return {
        isFetching: false,
        data: {
          token: '',
        },
        error: action.payload.response?.data.data,
        code: action.payload.response?.data.code,
      }
    case getType(actions.logoutSuccessAction):
      return {
        isFetching: false,
        data: {
          token: '',
        },
        error: '',
        code: 0,
      }
    default:
      return state
  }
}

export default loginReducer