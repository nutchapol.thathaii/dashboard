import { Epic } from 'redux-observable'
import { from, of } from 'rxjs'
import {
  catchError,
  exhaustMap,
  takeUntil,
  filter,
  mergeMap,
} from 'rxjs/operators'
import { isActionOf } from 'typesafe-actions'
import { fetchGetCreditInfo } from './services'
import actions from './actions'
import { RootAction } from 'typings/reduxs/Actions'

const getCreditInfoEpic: Epic<RootAction, RootAction, RootReducers> = (action$, store) =>
  action$.pipe(
    filter(isActionOf(actions.getCreditInfoListAction)),
    exhaustMap(action =>
      from(fetchGetCreditInfo(action.payload))
        .pipe(
          mergeMap(response => of(actions.getCreditInfoListSuccessAction(response, action.payload.isFirst || false))),
          catchError(error => of(actions.getCreditInfoListFailureAction(error))),
          takeUntil(action$.pipe(filter(isActionOf(actions.getCreditInfoListCancelAction))))
        ),
    )
  )
export default [
  getCreditInfoEpic,
]
