import axios from 'axios'
import { endpoint } from './constants'

export const fetchGetBetRatesAll = () => axios.get(endpoint.getBetRatesAll)
export const fetchPutBetRates = (putData: IBetRatesPutData[]) => {
    const putDatas = putData.map((data) => axios.put(endpoint.putBetRates, { ...data }))
    return axios.all(putDatas)
}
export const fetchGetYegeeGameConfig= () => axios.get(endpoint.yegeeGameConfig)