import { getType } from 'typesafe-actions'
import { RootAction } from 'typings/reduxs/Actions'
import { initialState } from './constants'
import actions from './actions'

const betRatesAllReducer = (state: IBetRatesAllState = initialState, action: RootAction): any => {
  switch (action.type) {
    case getType(actions.getBetRatesAllAction):
      return {
        ...state,
        isFetching: true,
      }
    case getType(actions.getBetRatesAllSuccessAction):
      return {
        isFetching: false,
        data: action.payload.data.data,
        code: action.payload.data.code,
      }

    case getType(actions.getBetRatesAllFailureAction):
      return {
        isFetching: false,
        error: action.payload.message,
        code: action.payload.code,
      }
    default:
      return state
  }
}

const betRatesPutReducer = (state: IPutBetRatesState = initialState, action: RootAction): any => {
  switch (action.type) {
    case getType(actions.putBetRatesAction):
      return {
        ...state,
        isFetching: true,
      }
    case getType(actions.putBetRatesSuccessAction):
      return {
        isFetching: false,
        data: [],
        code: 200,
      }

    case getType(actions.putBetRatesFailureAction):
      return {
        isFetching: false,
        error: action.payload.message,
        code: action.payload.code,
      }
    default:
      return state
  }
}

export {
  betRatesAllReducer,
  betRatesPutReducer,
}