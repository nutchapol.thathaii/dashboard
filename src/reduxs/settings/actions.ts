import allActions from './betRates/actions'
import yegeeConfig from './yegeeConfig/actions'


export default {
  ...allActions,
  ...yegeeConfig,
}