import { Epic } from 'redux-observable'
import { from, of } from 'rxjs'
import {
  catchError,
  exhaustMap,
  takeUntil,
  filter,
  map,
} from 'rxjs/operators'
import { isActionOf } from 'typesafe-actions'
import { RootAction } from 'typings/reduxs/Actions'
import { fetchGetYegeeGameConfig, fetchPutYegeeGameConfig } from './services'
import actions from './actions'

const yegeeGameConfigAllEpic: Epic<RootAction, RootAction, RootReducers> = (action$, store, dependencies) =>
  action$.pipe(
    filter(isActionOf(actions.getYegeeGameConfigAllAction)),
    exhaustMap(_ =>
      from(fetchGetYegeeGameConfig())
        .pipe(
          map(actions.getYegeeGameConfigAllSuccessAction),
          catchError(error => of(actions.getYegeeGameConfigAllFailureAction(error))),
          takeUntil(action$.pipe(filter(isActionOf(actions.getYegeeGameConfigAllCancelAction))))
        ),
    )
  )

const yegeeGameConfigPutEpic: Epic<RootAction, RootAction, RootReducers> = (action$, store, dependencies) =>
  action$.pipe(
    filter(isActionOf(actions.putYegeeGameConfigAction)),
    exhaustMap(action =>
      from(fetchPutYegeeGameConfig(action.payload))
        .pipe(
          map(actions.putYegeeGameConfigSuccessAction),
          catchError(error => of(actions.putYegeeGameConfigFailureAction(error))),
          takeUntil(action$.pipe(filter(isActionOf(actions.putYegeeGameConfigCancelAction))))
        ),
    )
  )


export {
  yegeeGameConfigAllEpic,
  yegeeGameConfigPutEpic,
}
