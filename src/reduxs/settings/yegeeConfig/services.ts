import axios from 'axios'
import { endpoint } from './constants'

export const fetchGetYegeeGameConfig = () => axios.get(endpoint.getYegeeGameConfig)
export const fetchPutYegeeGameConfig = (data: IPutYegeeConfigState) => axios.put(endpoint.putYegeeGameConfig, data)