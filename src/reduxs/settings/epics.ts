import { betRatesAllEpic, betRatesPutEpic } from './betRates/epics'
import { yegeeGameConfigAllEpic, yegeeGameConfigPutEpic } from './yegeeConfig/epics'
// import put from './check/epics'

export default [
  ...[betRatesAllEpic],
  ...[betRatesPutEpic],
  ...[yegeeGameConfigAllEpic],
  ...[yegeeGameConfigPutEpic],
]
