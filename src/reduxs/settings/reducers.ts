import { combineReducers } from 'redux'
import { betRatesAllReducer, betRatesPutReducer } from './betRates/reducers'
import { yegeeGameConfigReducer, putYegeeGameConfigReducer } from './yegeeConfig/reducers'
// import put from './check/reducers'

export default combineReducers({
  allBetRates: betRatesAllReducer,
  putBetRate: betRatesPutReducer,
  getYegeeConfig: yegeeGameConfigReducer,
  putYegeeConfig: putYegeeGameConfigReducer,
})