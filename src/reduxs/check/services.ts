import axios from 'axios'
import { endpoint } from './constants'

export const fetchPostCheckRateClient = (data: IPostCheckRateClient) =>
    axios.post(`${endpoint.postCheckRateClient}`, data)