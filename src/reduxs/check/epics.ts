import { Epic } from 'redux-observable'
import { from, of } from 'rxjs'
import {
  catchError,
  exhaustMap,
  takeUntil,
  filter,
  map,
} from 'rxjs/operators'
import { isActionOf } from 'typesafe-actions'
import { RootAction } from 'typings/reduxs/Actions'
import {
  fetchPostCheckRateClient,
} from './services'
import actions from './actions'

const checkRateClientEpic: Epic<RootAction, RootAction, RootReducers> = (action$, store, dependencies) =>
  action$.pipe(
    filter(isActionOf(actions.postCheckRateClientAction)),
    exhaustMap(action =>
      from(fetchPostCheckRateClient(action.payload))
        .pipe(
          map(actions.postCheckRateClientSuccessAction),
          catchError(error => of(actions.postCheckRateClientFailureAction(error))),
          takeUntil(action$.pipe(filter(isActionOf(actions.postCheckRateClientCancelAction))))
        ),
    )
  )

export default [
  checkRateClientEpic,
]
