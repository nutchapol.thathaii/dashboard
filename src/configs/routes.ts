import pages from 'pages'

const routes: IRoutes[] = [
  {
    exact: true,
    name: 'login',
    path: '/',
    component: pages.News,
  },
  {
    private: true,
    name: 'news',
    path: '/news',
    component: pages.News,
  },
]

export default routes