import moment from 'moment'
import localization from 'moment/locale/th'

moment.updateLocale('th', localization)
