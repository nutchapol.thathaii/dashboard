import { padStart } from 'lodash'
import { DECIMAL_DITGITS, COMMA } from 'constants/regex'

const castToInteger = (numberString: string = '') => {
  const result = numberString.replace(COMMA, '').replace(DECIMAL_DITGITS, '')
  return result
}
const padNumber = (value: string, ditgit: number) => {
  const padString = '0'
  return padStart(value, ditgit, padString)
}
const numberFormat = (data: string)=> {
  return  Intl.NumberFormat().format(Number(data))
}

export default {
  padNumber,
  castToInteger,
  numberFormat,
}