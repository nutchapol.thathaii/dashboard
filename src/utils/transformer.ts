import { camelCase, keys } from 'lodash'

const isArray = (array: any) => Array.isArray(array)

const isObject = (object: any) => object === Object(object) && !isArray(object) && typeof object !== 'function';

const camelcaseTransform = (data: any): { [name: string]: any } | [] => {
  if (isObject(data)) {
    const objectData = data as { [name: string]: any }
    const newObject: { [name: string]: any } = {}
    keys(objectData).forEach((key) => {
      newObject[camelCase(key)] = camelcaseTransform(objectData[key]);
    })
    return newObject
  } else if (isArray(data)) {
    const arrayData = data as []
    const newArray = arrayData.map((i) => camelcaseTransform(i))
    return newArray
  }
  return data
}

const calPercentage = (data: ITransformObjectNum2Percentage) => {
  for (const value of Object.keys(data)) {
    /* tslint:disable:no-bitwise */
    data[value] = String(Number(data[value]) * 100 | 0)
  }
  return data
}
const time1DigitTo2Digit = (time: string) => {
  if (time.length === 1) return `0${time}`
  return time
}

const seperateTimeString = (time: string): { hour: string; minutes: string } => {
  const timeSplit = time.split(':')
  return { hour: timeSplit[0], minutes: timeSplit[1] }
}

const typeFormat = (data: string, spacing?: boolean) => {
  const money = Intl.NumberFormat('th-TH', {
    style: 'currency',
    currency: 'THB',
  }).format(Number(data))
  if (spacing) {
    return money.replace(/^(\D+)/, '$1 ')
  }
  return money
}


export default {
  camelcaseTransform,
  time1DigitTo2Digit,
  seperateTimeString,
  typeFormat,
  calPercentage,
}