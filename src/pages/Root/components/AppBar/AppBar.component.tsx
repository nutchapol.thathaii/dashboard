import React, { Component } from 'react'
import { noop } from 'lodash'
import {
    AppBar,
    Toolbar,
    IconButton,
    Tooltip,

} from '@material-ui/core';
import {
    Notifications,
    Help,
    Input,
} from '@material-ui/icons'
import { THEME_MODE } from 'constants/variables'
import './AppBar.component.scss'

const defaultProps: IAppBarComponentProps = {
    menu: { activeControl: { menu: 'deposit' } },
    userMeResult: [],
    logout() { noop() },
    onChangeMenu() { noop() },
}

class AppBarComponent extends Component<IAppBarComponentProps, IAppBarStates> {

    static defaultProps = defaultProps
    state: IAppBarStates = {
        themeMode: THEME_MODE.DARK,
        hideMenu: false,
        consoleClassName: `content-container close`,
        appBarClassName: `app-bar close`,
        menuBarClassName: `menus-bar close`,
    }

    onOpenMenu = () => {
        const status = 'open'
        this.setState({
            hideMenu: false,
            consoleClassName: `content-container ${status}`,
            appBarClassName: `app-bar ${status}`,
            menuBarClassName: `menus-bar ${status}`,
        })
    }

    onCloseMenu = () => {
        const status = 'close'
        this.setState({
            hideMenu: false,
            consoleClassName: `content-container ${status}`,
            appBarClassName: `app-bar ${status}`,
            menuBarClassName: `menus-bar ${status}`,
        })
    }

    render() {
        return (
            <AppBar
                position="fixed"
                className="app-bar-container secondary-dark"
            >
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={() => { }}
                        edge="start"
                    >
                    
                        <h5 className="text-thin pd-l">Data Dashboard <span>
                            Thai Weaving Patten ImageGallery</span>
                        </h5>
                    </IconButton>
                    <div className="user-navigate-container">
                        <Tooltip title="แจ้งเตือน">
                            <IconButton
                                color="inherit"
                                aria-label="open drawer"
                                onClick={() => { }}
                                edge="start"
                            >
                                <Notifications />
                            </IconButton>
                        </Tooltip>
                        <Tooltip title="ข้อมูล">
                            <IconButton
                                color="inherit"
                                aria-label="open drawer"
                                onClick={() => { }}
                                edge="start"
                            >
                                <Help />
                            </IconButton>
                        </Tooltip>
                        <Tooltip title="ออกจากระบบ">
                            <IconButton
                                color="inherit"
                                aria-label="open drawer"
                                onClick={this.props.logout}
                                edge="start"
                            >
                                <Input />
                            </IconButton>

                        </Tooltip>
                    </div>
                </Toolbar>
            </AppBar>
        )
    }
}

export default AppBarComponent