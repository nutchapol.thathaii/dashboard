import React, { Component } from 'react'
import { noop } from 'lodash'
import Grid from '@material-ui/core/Grid'
import {
  Link,
} from 'react-router-dom'
import {
  Drawer,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  ListSubheader,
} from '@material-ui/core';
import { Chip } from 'components';
import SettingIcon from '@material-ui/icons/Settings'
import AccountBalanceIcon from '@material-ui/icons/AccountBalance'
import PermIdentityIcon from '@material-ui/icons/PermIdentity';
import AppSettingIcon from '@material-ui/icons/SettingsApplicationsOutlined'
import InderminateCheckBoxIcon from '@material-ui/icons/IndeterminateCheckBox'
import AddBoxIcon from '@material-ui/icons/AddBox'
import ReceiptIcon from '@material-ui/icons/Receipt'
import BarChartIcon from '@material-ui/icons/BarChart'
import GroupsIcon from '@material-ui/icons/Group'
import AirlineSeatReclineNormalIcon from '@material-ui/icons/AirlineSeatReclineNormal'
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount'
import AddPhotoAlternateIcon from '@material-ui/icons/AddPhotoAlternate';
import AssessmentIcon from '@material-ui/icons/Assessment';
import TuneIcon from '@material-ui/icons/Tune';
import SimpleBar from 'simplebar-react';
import 'simplebar/dist/simplebar.min.css';

import './MenuBar.component.scss'

type DefaultProps = Readonly<typeof defaultProps>

const defaultProps: IMenuBarProps & IMenuActionProps & IMenuBarComponentProps = {
  menu: { activeControl: { menu: 'deposit' } },
  userMeResult: [],
  userMeCode: 0,
  userMeError: '',
  userMeIsFetching: false,
  onChangeMenu() { noop() },
  notification: {
    depositTxWaitTotal: 0,
    withdrawTxWaitTotal: 0,
    depositTxReserveTotal: 0,
    withdrawTxReserveTotal: 0,
  },
}

class MenuBarComponent extends Component<IMenuBarProps & IMenuActionProps & DefaultProps, IMenuState> {

  static defaultProps = defaultProps
  state: IMenuState = {
    activeMenu: '',
  }

  renderAdminManagementMenu = (): JSX.Element => {
    return (
      <div className="sub-menu-bar">
        <Link to="/admin-management" onClick={() => this.props.onChangeMenu!('admin-management')}>
          <ListItem
            button
            key={'admin-management'}
            className={this.props.menu!.activeControl.menu === 'admin-management' ? 'primary-purple-text' : ''}
          >
            <ListItemIcon>
              <SupervisorAccountIcon
                className={
                  this.props.menu!.activeControl.menu === 'admin-management' ? 'primary-purple-text' : ''
                }
              />
            </ListItemIcon>
            <ListItemText primary={'จัดการหัวหน้าพนักงาน'} />
          </ListItem>
        </Link>
      </div>
    )
  }

  renderStaffManagementMenu = (): JSX.Element => {
    return (
      <div className="sub-menu-bar">
        <Link to="/staff-management" onClick={() => this.props.onChangeMenu!('staff-management')}>
          <ListItem
            button
            key={'staff-management'}
            className={this.props.menu!.activeControl.menu === 'staff-management' ? 'primary-purple-text' : ''}
          >
            <ListItemIcon>
              <AirlineSeatReclineNormalIcon
                className={
                  this.props.menu!.activeControl.menu === 'staff-management' ? 'primary-purple-text' : ''
                }
              />
            </ListItemIcon>
            <ListItemText primary={'จัดการพนักงาน'} />
          </ListItem>
        </Link>
      </div>
    )
  }

  renderUserManagementMenu = (): JSX.Element => {
    return (
      <div className="sub-menu-bar">
        <Link to="/user-management" onClick={() => this.props.onChangeMenu!('user-management')}>
          <ListItem
            button
            key={'user-management'}
            className={this.props.menu!.activeControl.menu === 'user-management' ? 'primary-purple-text' : ''}
          >
            <ListItemIcon>
              <GroupsIcon
                className={
                  this.props.menu!.activeControl.menu === 'user-management' ? 'primary-purple-text' : ''
                }
              />
            </ListItemIcon>
            <ListItemText primary={'จัดการผู้ใช้'} />
          </ListItem>
        </Link>
      </div>
    )
  }

  renderWebbankManagementMenu = (): JSX.Element => {
    return (
      <div className="sub-menu-bar">
        <Link to="/webbank" onClick={() => this.props.onChangeMenu!('webbank')}>
          <ListItem
            button
            key={'webbank'}
            className={this.props.menu!.activeControl.menu === 'webbank'
              ? 'primary-purple-text' : ''}
          >
            <ListItemIcon>
              <AccountBalanceIcon
                className={this.props.menu!.activeControl.menu === 'webbank'
                  ? 'primary-purple-text' : ''}
              />
            </ListItemIcon>
            <ListItemText primary={'บัญชีระบบ'} />
          </ListItem>
        </Link>
      </div>
    )
  }

  renderSumDashboardMenu = (): JSX.Element => {
    return (
      <div className="sub-menu-bar">
        <Link to="/summary-dashboard" onClick={() => this.props.onChangeMenu!('summary-dashboard')}>
          <ListItem
            button
            key={'summary'}
            className={this.props.menu!.activeControl.menu === 'summary-dashboard'
              ? 'primary-purple-text' : ''}
          >
            <ListItemIcon>
              <BarChartIcon
                className={this.props.menu!.activeControl.menu === 'summary-dashboard'
                  ? 'primary-purple-text' : ''}
              />
            </ListItemIcon>
            <ListItemText className="subtitle-1" primary={'ภาพรวม'} />
          </ListItem>
        </Link>
      </div>
    )
  }

  renderAffilateMenu = (): JSX.Element => {
    return (
      <div className="sub-menu-bar">
        <Link to="/Affilate" onClick={() => this.props.onChangeMenu!('members')}>
          <ListItem
            button
            key={'members'}
            className={this.props.menu!.activeControl.menu === 'members'
              ? 'primary-purple-text' : ''}
          >
            <ListItemIcon>
              <PermIdentityIcon
                className={this.props.menu!.activeControl.menu === 'members'
                  ? 'primary-purple-text' : ''}
              />
            </ListItemIcon>
            <ListItemText primary={'แนะนำสมาชิก'} />
          </ListItem>
        </Link>
      </div>
    )
  }

  renderSettinsgMenu = (): JSX.Element => {
    return (
      <div className="sub-menu-bar">
        <Link to="/settings" onClick={() => this.props.onChangeMenu!('settings')}>
          <ListItem
            button
            key={'settings'}
            className={this.props.menu!.activeControl.menu === 'settings'
              ? 'primary-purple-text' : ''}
          >
            <ListItemIcon>
              <SettingIcon
                className={this.props.menu!.activeControl.menu === 'settings'
                  ? 'primary-purple-text' : ''}
              />
            </ListItemIcon>
            <ListItemText primary={'ยี่กี'} />
          </ListItem>
        </Link>
      </div>
    )
  }

  renderWebConfigMenu = (): JSX.Element => {
    return (
      <div className="sub-menu-bar">
        <Link to="/web-config" onClick={() => this.props.onChangeMenu!('web-config')}>
          <ListItem
            button
            key={'web-config'}
            className={this.props.menu!.activeControl.menu === 'web-config'
              ? 'primary-purple-text' : ''}
          >
            <ListItemIcon>
              <AppSettingIcon
                className={this.props.menu!.activeControl.menu === 'web-config'
                  ? 'primary-purple-text' : ''}
              />
            </ListItemIcon>
            <ListItemText primary={'เว็บ'} />
          </ListItem>
        </Link>
      </div>
    )
  }
  renderDepositMenu = (): JSX.Element => {
    return (
      <div className="sub-menu-bar">
        <Link to="/transaction/deposit" onClick={() => this.props.onChangeMenu!('deposit')}>
          <ListItem
            button
            key={'transaction-management'}
            className={this.props.menu!.activeControl.menu === 'deposit' ? 'primary-purple-text' : ''}
          >
            <ListItemIcon>
              <AddBoxIcon
                className={this.props.menu!.activeControl.menu === 'deposit'
                  ? 'primary-purple-text' : ''}
              />
            </ListItemIcon>
            {/* <div> */}
            <ListItemText className="subtitle-1" primary={'รายการฝาก'} />
            {/* </div> */}


            <Grid container>
              <Grid item xs={5}>
                <Chip label={this.props.notification.depositTxWaitTotal} color="default" />
              </Grid>
              <Grid item xs={2}>
                {}
              </Grid>
              <Grid item xs={5}>
                <Chip label={this.props.notification.depositTxReserveTotal} color="gray" />
              </Grid>
            </Grid>
          </ListItem>
        </Link>
      </div>
    )
  }
  renderWithdrawMenu = (): JSX.Element => {
    return (
      <div className="sub-menu-bar">
        <Link to="/transaction/withdraw" onClick={() => this.props.onChangeMenu!('withdraw')}>
          <ListItem
            button
            key={'withdraw'}
            className={this.props.menu!.activeControl.menu === 'withdraw'
              ? 'primary-purple-text' : ''}
          >
            <ListItemIcon>
              <InderminateCheckBoxIcon
                className={this.props.menu!.activeControl.menu === 'withdraw'
                  ? 'primary-purple-text' : ''}
              />
            </ListItemIcon>
            <ListItemText className="subtitle-1" primary={'รายการถอน'} />
            <Grid container>
              <Grid item xs={5}>
                <Chip label={this.props.notification.withdrawTxWaitTotal} color="default" />
              </Grid>
              <Grid item xs={2}>
                {}
              </Grid>
              <Grid item xs={5}>
                <Chip label={this.props.notification.withdrawTxReserveTotal} color="gray" />
              </Grid>
            </Grid>
          </ListItem>
        </Link>
      </div>
    )
  }

  renderNewsMenu = (): JSX.Element => {
    return (
      <div className="sub-menu-bar">
        <Link to="/news" onClick={() => this.props.onChangeMenu!('news')}>
          <ListItem
            button
            key={'news'}
            className={this.props.menu!.activeControl.menu === 'news'
              ? 'primary-purple-text' : ''}
          >
            <ListItemIcon>
              <ReceiptIcon
                className={this.props.menu!.activeControl.menu === 'news' ? 'primary-purple-text' : ''}
              />
            </ListItemIcon>
            <ListItemText primary={'ข่าวสาร'} />
          </ListItem>
        </Link>
      </div>
    )
  }

  renderLottoList = (): JSX.Element => {
    return (
      <div className="sub-menu-bar">
        <Link to="/lotto" onClick={() => this.props.onChangeMenu!('lotto')}>
          <ListItem
            button
            key={'lotto'}
            className={this.props.menu!.activeControl.menu === 'lotto'
              ? 'primary-purple-text' : ''}
          >
            <ListItemIcon>
              <TuneIcon
                className={this.props.menu!.activeControl.menu === 'lotto' ? 'primary-purple-text' : ''}
              />
            </ListItemIcon>
            <ListItemText primary={'หวย'} />
          </ListItem>
        </Link>
      </div>
    )
  }

  renderLottoRate = (): JSX.Element => {
    return (
      <div className="sub-menu-bar">
        <Link to="/lotto-rate" onClick={() => this.props.onChangeMenu!('lotto-rate')}>
          <ListItem
            button
            key={'lottorate'}
            className={this.props.menu!.activeControl.menu === 'lotto-rate'
              ? 'primary-purple-text' : ''}
          >
            <ListItemIcon>
              <ReceiptIcon
                className={this.props.menu!.activeControl.menu === 'lotto-rate'
                  ? 'primary-purple-text'
                  : ''}
              />
            </ListItemIcon>
            <ListItemText primary={'อัตราจ่ายหวย'} />
          </ListItem>
        </Link>
      </div>
    )
  }

  renderContent = (): JSX.Element => {
    return (
      <div className="sub-menu-bar">
        <Link to="/content" onClick={() => this.props.onChangeMenu!('content')}>
          <ListItem
            button
            key={'content'}
            className={this.props.menu!.activeControl.menu === 'content'
              ? 'primary-purple-text' : ''}
          >
            <ListItemIcon>
              <AddPhotoAlternateIcon
                className={this.props.menu!.activeControl.menu === 'content'
                  ? 'primary-purple-text'
                  : ''}
              />
            </ListItemIcon>
            <ListItemText primary={'จัดการโฆษณา'} />
          </ListItem>
        </Link>
      </div>
    )
  }

  renderLottoMasterManagementMenu = (): JSX.Element => {
    return (
      <div className="sub-menu-bar">
        <Link to="/lotter-master-management" onClick={() => this.props.onChangeMenu!('lotter-master-management')}>
          <ListItem
            button
            key={'lotter-master-management'}
            className={this.props.menu!.activeControl.menu === 'lotter-master-management' ? 'primary-purple-text' : ''}
          >
            <ListItemIcon>
              <GroupsIcon
                className={
                  this.props.menu!.activeControl.menu === 'lotter-master-management' ? 'primary-purple-text' : ''
                }
              />
            </ListItemIcon>
            <ListItemText primary={'จัดการพนักงานควบคุมหวย'} />
          </ListItem>
        </Link>
      </div>
    )
  }

  renderReportYegeeMenu = (): JSX.Element => {
    return (
      <div className="sub-menu-bar">
        <Link to="/report-yegee" onClick={() => this.props.onChangeMenu!('report-yegee')}>
          <ListItem
            button
            key={'report-yegee'}
            className={this.props.menu!.activeControl.menu === 'report-yegee' ? 'primary-purple-text' : ''}
          >
            <ListItemIcon>
              <AssessmentIcon
                className={
                  this.props.menu!.activeControl.menu === 'report-yegee' ? 'primary-purple-text' : ''
                }
              />
            </ListItemIcon>
            <ListItemText primary={'รายงานผลยี่กี'} />
          </ListItem>
        </Link>
      </div>
    )
  }

  renderManagement = (): JSX.Element => {
    return (
      <ListSubheader disableSticky={true}>
        <div className="group-title subtitle-1 secondary-text"> จัดการระบบ </div>
      </ListSubheader>
    )
  }

  renderReportTitle = (): JSX.Element => {
    return (
      <ListSubheader disableSticky={true}>
        <div className="group-title subtitle-1 secondary-text"> รายงาน </div>
      </ListSubheader>
    )
  }

  renderSettingsTitle = (): JSX.Element => {
    return (
      <ListSubheader disableSticky={true}>
        <div className="group-title subtitle-1 secondary-text"> ตั้งค่าระบบ </div>
      </ListSubheader>
    )
  }

  renderUserManagement = (): JSX.Element => {
    const RenderManagement = this.renderManagement
    const RenderDepositMenu = this.renderDepositMenu
    const RenderWithdrawMenu = this.renderWithdrawMenu
    const RenderWebbankManagementMenu = this.renderWebbankManagementMenu
    const RenderAdminManagementMenu = this.renderAdminManagementMenu
    const RenderStaffManagementMenu = this.renderStaffManagementMenu
    const RenderUserManagementMenu = this.renderUserManagementMenu
    const RenderLottoMasterManagementMenu = this.renderLottoMasterManagementMenu
    const RenderNewsMenu = this.renderNewsMenu
    const RenderLottoList = this.renderLottoList
    // const RenderReportYegeeMenu = this.renderReportYegeeMenu
    // const RenderContent = this.renderContent

    switch (this.props.userMeResult.permission) {
      case 'SUPER_ADMIN':
        return (
          <>
            <RenderManagement />
            <RenderDepositMenu />
            <RenderWithdrawMenu />
            <RenderWebbankManagementMenu />
            <RenderAdminManagementMenu />
            <RenderStaffManagementMenu />
            <RenderUserManagementMenu />
            <RenderLottoMasterManagementMenu />
            <RenderNewsMenu />
            <RenderLottoList />
            {/* <RenderReportYegeeMenu /> */}
            {/* <RenderContent /> */}
          </>
        )
      case 'ADMIN':
        return (
          <>
            <RenderManagement />
            <RenderDepositMenu />
            <RenderWithdrawMenu />
            <RenderWebbankManagementMenu />
            <RenderStaffManagementMenu />
            <RenderUserManagementMenu />
            <RenderLottoMasterManagementMenu />
            <RenderNewsMenu />
            <RenderLottoList />
          </>
        )
      case 'STAFF':
        return (
          <>
            <RenderManagement />
            <RenderDepositMenu />
            <RenderWithdrawMenu />
            <RenderUserManagementMenu />
          </>
        )
      case 'LOTTER_MASTER':
        return (
          <>
            <RenderLottoList />
          </>
        )
      default:
        return <></>
    }
  }

  renderReportManagement = (): JSX.Element => {
    const RenderReportTitle = this.renderReportTitle
    const RenderSumDashboardMenu = this.renderSumDashboardMenu

    switch (this.props.userMeResult.permission) {
      case 'SUPER_ADMIN':
        return (
          <>
            <RenderReportTitle />
            <RenderSumDashboardMenu />
          </>
        )
      case 'ADMIN':
        return (
          <>
          </>
        )
      case 'STAFF':
        return (
          <>
          </>
        )
      default:
        return <></>
    }
  }

  renderSettingsManagement = (): JSX.Element => {
    const RenderSettingsTitle = this.renderSettingsTitle
    const RenderAffilateMenu = this.renderAffilateMenu
    // const RenderSettinsgMenu = this.renderSettinsgMenu
    const RenderWebConfigMenu = this.renderWebConfigMenu
    const RenderLottoRate = this.renderLottoRate
    switch (this.props.userMeResult.permission) {
      case 'SUPER_ADMIN':
        return (
          <>
            <RenderSettingsTitle />
            <RenderAffilateMenu />
            {/* <RenderSettinsgMenu /> */}
            <RenderWebConfigMenu />
            <RenderLottoRate />
          </>
        )
      case 'ADMIN':
        return (
          <>
            <RenderSettingsTitle />
            <RenderLottoRate />
          </>
        )
      case 'STAFF':
        return (
          <>
          </>
        )
      default:
        return <></>
    }
  }



  render() {
    const RenderReportManagement = this.renderReportManagement
    const RenderUserManagement = this.renderUserManagement
    const RenderSettingsManagement = this.renderSettingsManagement
    return (
      <Drawer
        variant="permanent"
        className="menu-bar-container"
      >
        <List>
          <SimpleBar style={{ height: '100%' }}>
            <RenderReportManagement />
            <RenderUserManagement />
            <RenderSettingsManagement />
          </SimpleBar>
        </List>
      </Drawer>
    )
  }
}

export default MenuBarComponent