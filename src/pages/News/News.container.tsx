import React, { Component } from 'react'
import { RouteComponentProps } from 'react-router-dom'
import { map } from 'lodash'
import {
  Grid,
} from '@material-ui/core'
import { noop } from 'lodash'
import './News.container.scss'
import Gallery from 'react-photo-gallery';
import { Data } from './data'
import { photos } from './photo'
import { GoogleMap, withScriptjs, withGoogleMap, Marker, InfoWindow } from 'react-google-maps';
import { Button } from 'components'
import {
  LottoActionCard,
} from 'components'

import SimpleBar from 'simplebar-react'
import 'simplebar/dist/simplebar.min.css'




type DefaultProps = Readonly<typeof defaultProps>

const defaultProps: INewsContainerProps & INewsContainerActionProps = {
  getNewsAll() { noop() },
  getNewsAllCode: 0,
  getNewsAllError: '',
  getNewsAllIsFetching: false,
  getNewsAllResult: [],
  postNews() { noop() },
  postNewsCode: 0,
  postNewsError: '',
  postNewsIsFetching: false,
  postNewsResult: [],
  putNews() { noop() },
  putNewsCode: 0,
  putNewsError: '',
  putNewsIsFetching: false,
  putNewsResult: [],
  deleteNews() { noop() },
  deleteNewsCode: 0,
  deleteNewsError: '',
  deleteNewsIsFetching: false,
  deleteNewsResult: [],
  loader() { noop() },
}

class NewsContainer extends
  Component<INewsContainerProps & INewsContainerActionProps & DefaultProps & RouteComponentProps, INewsContainerState> {

  static defaultProps = defaultProps

  constructor(props: INewsContainerProps & INewsContainerActionProps & DefaultProps & RouteComponentProps) {
    super(props)
    this.state = {
      newsList: [],
      onEdit: false,
      editTitle: '',
      editDescription: '',
      insertTitle: '',
      insertDescription: '',
      setselectedPar: null,
      lat: 18.49244788367628,
      lng: 98.36272718427986,
      thaiName: 'ผ้าจกแม่แจ่ม',
      parArray: [],
      flagClose: false,
      id: '',
      sector: Data.North,
      north: false,
      northeast: false,
      central: false,
      south: false,
    }
  }

  componentDidMount() {
    // this.props.getNewsAll()
    // this.props.loader(true)
  }

  componentDidUpdate(prevProps: INewsContainerProps) {
    if (prevProps.getNewsAllIsFetching !== this.props.getNewsAllIsFetching
      && !this.props.getNewsAllIsFetching) {
      this.props.loader(false)
    }
  }



  Map = () => {
    return (
      <GoogleMap
        defaultZoom={17}
        defaultCenter={{ lat: this.state.lat, lng: this.state.lng }}
      >
        <Marker
          key={this.state.id}
          position={{
            lat: this.state.lat,
            lng: this.state.lng,
          }}
          onClick={() => {
            this.setState({ flagClose: true })
          }}
        />
        {this.state.flagClose && (
          (
            <InfoWindow
              key={this.state.id}
              position={{
                lat: Number(this.state.lat),
                lng: Number(this.state.lng),
              }}
              onCloseClick={() => {
                this.setState({ flagClose: false })
              }}
            >
              <div>
                {this.state.thaiName}
              </div>
            </InfoWindow>
          )
        )
        })
      </GoogleMap>
    )
  }

  renderPar = (data: any) => {
    this.setState({
      id: data.silk.id,
      lat: Number(data.silk.Latitude),
      lng: Number(data.silk.Longitude),
      parArray: data,
      thaiName: data.silk.thaiName,
    })
  }


  renderLottoList = () => map(this.state.sector, (data, index) => {
    console.log(data)

    return (
      <div className="lotto-body-item" key={`lotto-2`}>
        <LottoActionCard
          id={'1'}
          title={data.silk.thaiName}
          icon={data.silk.image[0]}
          subTitle={data.silk.engName}
          status={'OPEN'}
          onClick={() => this.renderPar(data)}
        />
      </div>
    )
  })

  onActionRenderMap = (key:string) => {
    if(key === 'North') {
      this.setState(
        {sector: Data.North,
         north: true,
         northeast: false,
         central: false,
         south: false,
        })
    }
    else if (key === 'Northeast') {
      this.setState(
        {sector: Data.Northeast,
          north: false,
          northeast: true,
          central: false,
          south: false,
        })
    }
    else if (key === 'Central') {
      this.setState({sector: Data.Central,
        north: false,
        northeast: false,
        central: true,
        south: false,
      })
    }
    else if (key === 'South') {
      this.setState({sector: Data.South,
        north: false,
        northeast: false,
        central: false,
        south: true,
      })
    }
  }

  render() {
    const WapperedMap = withScriptjs(withGoogleMap(this.Map))
    const RenderLottoList = this.renderLottoList()
    return (
      <div className="news-container">
        <div className="element">
          <Grid container spacing={3}>
            <Grid item xs={7}>
              <div style={{ width: '100%', height: '400px' }}>
                <WapperedMap
                  // tslint:disable-next-line:comment-format
                  //@ts-ignore
                  googleMapURL={`https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places
                  &key=AIzaSyBHsNqU6ldYUH4giVcm50t0yE89X1eE4zg`}
                  // @ts-ignore
                  loadingElement={<div style={{ height: `100%` }} />}
                  // @ts-ignore
                  containerElement={<div style={{ height: `525px` }} />}
                  // @ts-ignore
                  mapElement={<div style={{ height: `100%`, color: 'red' }} />}
                />
              </div>
            </Grid>
            <Grid item xs={5}>
              <div className="lotto-list-container primary-bg">
                <div className="lotto-body1">
                  <SimpleBar style={{ height: '100%' }}>
                    <div className="m0-x m1-y">
                    <Grid container spacing={3}>
                      <Grid item xs={3}>
                        <Button
                          text="ภาคเหนือ"
                          fullWidth={true}
                          color={this.state.north === true ? 'green' : undefined}
                          onClick={() => this.onActionRenderMap('North')}
                        />
                      </Grid>
                      <Grid item xs={3}>
                        <Button
                          text="
                          ภาคตะวันออกเฉียงเหนือ"
                          fullWidth={true}
                          onClick={() => this.onActionRenderMap('Northeast')}
                          color={this.state.northeast === true ? 'green' : undefined}
                        />
                      </Grid>
                      <Grid item xs={3}>
                        <Button
                          text="ภาคกลาง"
                          fullWidth={true}
                          onClick={() => this.onActionRenderMap('Central')}
                          color={this.state.central === true ? 'green' : undefined}
                        />
                      </Grid>
                      <Grid item xs={3}>
                        <Button
                          text="ภาคใต้"
                          fullWidth={true}
                          onClick={() => this.onActionRenderMap('South')}
                          color={this.state.south === true ? 'green' : undefined}
                        />
                      </Grid>
                    </Grid>
                    </div>
                    {RenderLottoList}
                  </SimpleBar>
                </div>
              </div>

            </Grid>
          </Grid>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Gallery photos={photos} />
            </Grid>
          </Grid>
        </div>
      </div>
    )
  }
}

export default NewsContainer