import React, { useState } from 'react';
import { storiesOf } from '@storybook/react';
import SearchIcon from '@material-ui/icons/Search'
import TextField from './TextField.component';
import { noop } from 'lodash';

storiesOf('TextField', module)
    .add('Default',
        () => {
            const [value, setValue] = useState('')
            return (
                <TextField
                    value={value}
                    onChange={(e) => setValue(e.target.value)}
                />
            )
        }
    )
    .add('Default With Start Icon',
        () => {
            const [value, setValue] = useState('')
            return (
                <TextField
                    value={value}
                    startElement={<SearchIcon />}
                    onClick={() => { noop() }}
                    onBlur={() => { noop() }}
                    onChange={(e) => setValue(e.target.value)}
                />
            )
        }
    )
    .add('Default With End Icon',
        () => {
            const [value, setValue] = useState('')
            return (
                <TextField
                    value={value}
                    endElement={<SearchIcon />}
                    onClick={() => { noop() }}
                    onBlur={() => { noop() }}
                    onChange={(e) => setValue(e.target.value)}
                />
            )
        }
    )
    .add('Full props',
        () => {
            const [value, setValue] = useState('')
            return (
                <TextField
                    id="test"
                    name="test"
                    type="text"
                    label="text label"
                    error={false}
                    fullWidth={true}
                    helperText="is test input"
                    placeholder="please input"
                    value={value}
                    startElement={<SearchIcon />}
                    endElement={<SearchIcon />}
                    onClick={() => { noop() }}
                    onBlur={() => { noop() }}
                    onChange={(e) => setValue(e.target.value)}
                />
            )
        }
    )
    .add('Error',
        () => {
            const [value, setValue] = useState('')
            return (
                <TextField
                    value={value}
                    error={true}
                    helperText={'Test Help Text'}
                    onChange={(e) => setValue(e.target.value)}
                />
            )
        }
    )
    .add('Error With Start Icon',
        () => {
            const [value, setValue] = useState('')
            return (
                <TextField
                    value={value}
                    error={true}
                    startElement={<SearchIcon />}
                    onClick={() => { noop() }}
                    onBlur={() => { noop() }}
                    onChange={(e) => setValue(e.target.value)}
                />
            )
        }
    )
    .add('Error With End Icon',
        () => {
            const [value, setValue] = useState('')
            return (
                <TextField
                    value={value}
                    error={true}
                    endElement={<SearchIcon />}
                    onClick={() => { noop() }}
                    onBlur={() => { noop() }}
                    onChange={(e) => setValue(e.target.value)}
                />
            )
        }
    )
    .add('Error With Start and End Icon',
        () => {
            const [value, setValue] = useState('')
            return (
                <TextField
                    value={value}
                    error={true}
                    startElement={<SearchIcon />}
                    endElement={<SearchIcon />}
                    onClick={() => { noop() }}
                    onBlur={() => { noop() }}
                    onChange={(e) => setValue(e.target.value)}
                />
            )
        }
    )