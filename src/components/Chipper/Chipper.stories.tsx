import React from 'react';
import { storiesOf } from '@storybook/react';
import Chipper from './Chipper.component';

storiesOf('Chipper', module)
    .add('Green',
        () => (
            <Chipper label="อนุมัติ" color="green" />
        )
    )
    .add('Red',
        () => (
            <Chipper label="ไม่อนุมัติ" color="red" />
        )
    )
    .add('Orange',
        () => (
            <Chipper label="รอตรวจสอบ" color="yellow" />
        )
    )