import React from 'react'
import emiter from 'configs/emiter'
import event from 'constants/event'
import { ModalProps } from 'react-bootstrap/Modal'
import Modal from './Modal.component'
import { Success } from './components/Success'
import { Error } from './components/Error'
import { Accepts } from './components/Accepts'
import { TransactionItemCollapsible }  from './components/TransactionItemCollapsible'

const success = {
  show: ({ action, actionText, description, title }: ISuccessModal) => {
    const modalProps: ModalProps = {
      size: 'sm',
    }
    return emiter.emit(event.MODAL, {
      state: 'show',
      extraProps: modalProps,
      component: (
        <Success
          title={title}
          description={description}
          action={action}
          actionText={actionText}
        />
      ),
    })
  },
  hide: () => emiter.emit(event.MODAL, { state: 'hide' }),
}

const error = {
  show: ({ action, actionText, description, title }: IErrorModal) => {
    const modalProps: ModalProps = {
      size: 'sm',
    }
    return emiter.emit(event.MODAL, {
      state: 'show',
      extraProps: modalProps,
      component: (
        <Error
          title={title}
          description={description}
          action={action}
          actionText={actionText}
        />
      ),
    })
  },
  hide: () => emiter.emit(event.MODAL, { state: 'hide' }),
}

const accepts = {
  show: ({ action, cancelAction, title, twoDown, threeUp, fourSuit}: IAcceptsModal) => {
    const modalProps: ModalProps = {
      size: 'sm',
    }
    return emiter.emit(event.MODAL, {
      state: 'show',
      extraProps: modalProps,
      component: (
        <Accepts
          action={action}
          cancelAction={cancelAction}
          title={title}
          twoDown={twoDown}
          threeUp={threeUp}
          fourSuit={fourSuit}
        />
      ),
    })
  },
  hide: () => {
    emiter.emit(event.MODAL, { state: 'hide' })
  },
}

const transactionItemCollapsible = {
  show: ({ action, cancelAction, title, twoDown, threeUp, fourSuit, ...props}: ITransactionItemCollapsibleModal) => {
    const modalProps: ModalProps = {
      size: 'sm',
    }
    return emiter.emit(event.MODAL, {
      state: 'show',
      extraProps: modalProps,
      component: (
        <TransactionItemCollapsible
          hasMore={props.hasMore}
          creditInfo={props.creditInfo}
          action={action}
          cancelAction={cancelAction}
          money={props.money}
          title={title}
          twoDown={twoDown}
          threeUp={threeUp}
          fourSuit={fourSuit}
          creditName={props.creditName}
        />
      ),
    })
  },
  hide: () => {
    emiter.emit(event.MODAL, { state: 'hide' })
  },
}

const transactionItemCollapsibleUpdate = {
  show: ({ action, cancelAction, title, twoDown, threeUp, fourSuit, ...props}: ITransactionItemCollapsibleModal) => {
    const modalProps: ModalProps = {
      size: 'sm',
    }
    return emiter.emit(event.MODAL, {
      state: 'update',
      extraProps: modalProps,
      component: (
        <TransactionItemCollapsible
          hasMore={props.hasMore}
          creditInfo={props.creditInfo}
          action={action}
          cancelAction={cancelAction}
          money={props.money}
          title={title}
          twoDown={twoDown}
          threeUp={threeUp}
          fourSuit={fourSuit}
          creditName={props.creditName}
        />
      ),
    })
  },
  hide: () => {
    emiter.emit(event.MODAL, { state: 'hide' })
  },
}

export default {
  Core: Modal,
  success,
  error,
  accepts,
  transactionItemCollapsible,
  transactionItemCollapsibleUpdate,
}