import React, { SFC } from 'react'
import { noop } from 'lodash'
import './success.style.scss'


const constants = {
  title: 'เรียบร้อย',
  description: 'กรุณารอการตรวจสอบสักครู่',
  approval: 'อนุมัติ',
  cancel: 'ยกเลิก',
  bank: 'ธนาคาร: ',
  accountNo: 'หมายเลขบัญชี: ',
  amount: 'จำนวนเงิน: ',
  baht: ' บาท',
}

type DefaultProps = Readonly<typeof defaultProps>

const defaultProps: ISuccessModal = {
  title: constants.title,
  description: constants.description,
  action() { noop() },
}

const SuccessModal: SFC<ISuccessModal & DefaultProps> = (props: ISuccessModal) => {

  return (
<div>
      {}
    </div>
  )
}

SuccessModal.defaultProps = defaultProps

export default SuccessModal