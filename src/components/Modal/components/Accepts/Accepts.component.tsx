import React, { SFC } from 'react'
import { noop, map } from 'lodash'
import './success.style.scss'
import {
  Grid
} from '@material-ui/core'
// import CancelIcon from '@material-ui/icons/Cancel';
import { Button, ResponsiveIcon } from 'components'
import ErrorIcon from 'assets/images/modal/error/error.png'
import ErrorIcon2x from 'assets/images/modal/error/error@2x.png'
import ErrorIcon3x from 'assets/images/modal/error/error@3x.png'

const constants = {
  description: 'ยืนยันการทำรายการ',
  accepts: 'ยืนยัน',
  cancel: 'ยกเลิก',
  twoDown: '2 ตัวล่าง',
  threeUp: '3 ตัวบน',
  fourSuit: 'เลขชุด 4 ตัว',
}

type DefaultProps = Readonly<typeof defaultProps>

const defaultProps: IAcceptsModal = {
  description: constants.description,
  action() { noop() },
  cancelAction() { noop() },
  fourSuit: '',
  twoDown: '',
  threeUp: '',
}

const AcceptsModal: SFC<IAcceptsModal & DefaultProps> = (props: IAcceptsModal) => {

  const lottoNumber = () => {
    return map(props, (value, key) => {
      if ((key === 'threeUp' || key === 'twoDown' || key === 'fourSuit')
        && (value !== 'undefined' && value !== '')) {
        return (
          <h4 className="d-flex justify-content-center">
            {constants[key]}
            {(
              <div className="primary-green-text lotto-number-text m1-l">
                {` ${value}`}
              </div>
            )}
          </h4>
        )
      }
    });
  }

  let containerClass = `accepts-modal-container`
      containerClass = `${containerClass} ${props.fourSuit ? 'height-lotto-suit' : props.threeUp ? 'height-lotto' : ''}`

  return (
    <div className={containerClass}>
      <div className="description-error-modal">
        <ResponsiveIcon
          alt="error-icon"
          className="error-checked-icon"
          icon={{ x1: ErrorIcon, x2: ErrorIcon2x, x3: ErrorIcon3x }}
        />
        <h5 className="subtitle-2 m3-b">{`${props.description} ${props.title}`}</h5>
        {lottoNumber()}
        <div className="m3-t">
          <Grid container justify="center" spacing={2}>
            <Grid item xs={6}>
              <div className="footer-wrapper-success-modal">
                <Button
                  onClick={props.cancelAction}
                  text={constants.cancel}
                  color="red"
                />
              </div>
            </Grid>
            <Grid item xs={6}>
              <div className="footer-wrapper-success-modal">
                <Button
                  onClick={props.action}
                  text={constants.accepts}
                  color="green"
                />
              </div>
            </Grid>
          </Grid>
        </div>
      </div>
    </div>
  )
}

AcceptsModal.defaultProps = defaultProps

export default AcceptsModal