import React, { useState } from 'react';
import { storiesOf } from '@storybook/react';
import Select from './Select.component';
import SearchIcon from '@material-ui/icons/Search'
import ThailandImage from 'assets/images/flags/thailand.png'
import bank from 'assets/images/global/bank'
import { noop } from 'lodash';

storiesOf('Select', module)
    .add('Default',
        () => {
            const [value, setValue] = useState('10')

            return (
                <div style={{ backgroundColor: '#282C34', padding: '20px' }}>
                    <Select
                        label="test select"
                        value={value}
                        onChange={(e) => setValue(e.target.value)}
                        isMultiline={false}
                        itemList={[
                            { value: '', element: <h6>None</h6> },
                            { value: '10', element: <h6>Ten</h6> },
                            { value: '20', element: <h6>Twenty</h6> },
                            { value: '30', element: <h6>Thirty</h6> },
                        ]}
                    />
                </div>
            )
        }
    )
    .add('Error',
        () => {
            const [value, setValue] = useState('10')

            return (
                <Select
                    label="test select"
                    value={value}
                    onChange={(e) => setValue(e.target.value)}
                    error={true}
                    itemList={[
                        { value: '', element: <h6>None</h6> },
                        { value: '10', element: <h6>Ten</h6> },
                        { value: '20', element: <h6>Twenty</h6> },
                        { value: '30', element: <h6>Thirty</h6> },
                    ]}
                />
            )
        }
    )
    .add('Default With Start Icon',
        () => {
            const [value, setValue] = useState('10')
            return (
                <Select
                    value={value}
                    startElement={<SearchIcon />}
                    onClick={() => { noop() }}
                    onBlur={() => { noop() }}
                    onChange={(e) => setValue(e.target.value)}
                />
            )
        }
    )
    .add('Default Multiline',
        () => {
            const [value, setValue] = useState('10')

            return (
                <div style={{ backgroundColor: '#282C34', padding: '20px' }}>
                    <Select
                        label="test select"
                        value={value}
                        onChange={(e) => setValue(e.target.value)}
                        isMultiline={true}
                        itemList={[
                            { value: '10', element: <div style={{ display: 'grid' }}><h6>10</h6><h6>Two</h6></div> },
                            { value: '20', element: <div style={{ display: 'grid' }}><h6>20</h6><h6>Two</h6></div> },
                            { value: '30', element: <div style={{ display: 'grid' }}><h6>30</h6><h6>Two</h6></div> },
                            { value: '40', element: <div style={{ display: 'grid' }}><h6>40</h6><h6>Two</h6></div> },
                        ]}
                    />
                </div>
            )
        }
    )
    .add('Default Multiline With Image',
        () => {
            const [value, setValue] = useState('10')

            return (
                <div style={{ backgroundColor: '#282C34', padding: '20px' }}>
                    <Select
                        label="test select"
                        value={value}
                        onChange={(e) => setValue(e.target.value)}
                        isMultiline={true}
                        itemList={[
                            {
                                value: '10', element: (
                                    <div>
                                        <div style={{ display: 'flex' }}>
                                            <div style={{ width: '40px', height: '40px', marginRight: '16px' }}>
                                                <img
                                                    src={ThailandImage}
                                                    style={{ width: 'inherit', height: 'inherit' }}
                                                    alt="thailand-flat"
                                                />
                                            </div>
                                            <div style={{ display: 'grid' }}>
                                                <h6>10</h6><h6>Two</h6>
                                            </div>
                                        </div>
                                    </div>
                                ),
                            },
                            {
                                value: '20', element: (
                                    <div>
                                        <div style={{ display: 'flex' }}>
                                            <div style={{ width: '40px', height: '40px', marginRight: '16px' }}>
                                                <img
                                                    src={bank.BAY.Icon}
                                                    style={{ width: 'inherit', height: 'inherit' }}
                                                    alt="thailand-flat"
                                                />
                                            </div>
                                            <div style={{ display: 'grid' }}>
                                                <h6>10</h6><h6>Two</h6>
                                            </div>
                                        </div>
                                    </div>
                                ),
                            },
                            {
                                value: '30', element: (
                                    <div>
                                        <div style={{ display: 'flex' }}>
                                            <div style={{ width: '40px', height: '40px', marginRight: '16px' }}>
                                                <img
                                                    src={bank.SCB.Icon}
                                                    style={{ width: 'inherit', height: 'inherit' }}
                                                    alt="thailand-flat"
                                                />
                                            </div>
                                            <div style={{ display: 'grid' }}>
                                                <h6>10</h6><h6>Two</h6>
                                            </div>
                                        </div>
                                    </div>
                                ),
                            },
                            {
                                value: '40', element: (
                                    <div>
                                        <div style={{ display: 'flex' }}>
                                            <div style={{ width: '40px', height: '40px', marginRight: '16px' }}>
                                                <img
                                                    src={bank.KTB.Icon}
                                                    style={{ width: 'inherit', height: 'inherit' }}
                                                    alt="thailand-flat"
                                                />
                                            </div>
                                            <div style={{ display: 'grid' }}>
                                                <h6>10</h6><h6>Two</h6>
                                            </div>
                                        </div>
                                    </div>
                                ),
                            },
                        ]}
                    />
                </div>
            )
        }
    )