import React from 'react';
import { storiesOf } from '@storybook/react';
import Snackbar from './SnackbarCore.component';

storiesOf('Snackbar', module)
    .add('info',
        () => (
            <div>
                <button onClick={() => Snackbar.info.show({ message: 'info' })}>info</button>
                <Snackbar.Core />
            </div>
        )
    )
    .add('success',
        () => (
            <div>
                <button onClick={() => Snackbar.success.show({ message: 'success' })}>success</button>
                <Snackbar.Core />
            </div>
        )
    )
    .add('warning',
        () => (
            <div>
                <button onClick={() => Snackbar.warning.show({ message: 'warning' })}>warning</button>
                <Snackbar.Core />
            </div>
        )
    )
    .add('error',
        () => (
            <div>
                <button onClick={() => Snackbar.error.show({ message: 'error' })}>error</button>
                <Snackbar.Core />
            </div>
        )
    )