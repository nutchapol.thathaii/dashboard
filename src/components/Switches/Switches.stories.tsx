import React from 'react';
import { storiesOf } from '@storybook/react';
import Switches from './Switches.component';

storiesOf('Switches', module)
    .add('Small default',
        () => (
            <Switches checked={true} name={'123'} />
        )
    )