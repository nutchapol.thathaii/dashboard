import React from 'react';
import { storiesOf } from '@storybook/react';
import Button from './Button.component';

storiesOf('Button', module)
    .add('default',
        () => (
            <Button
                text="BUTTON"
            />
        )
    )
    .add('outline',
        () => (
            <Button
                text="BUTTON"
                theme="outline"
            />
        )
    )
    .add('outline red',
        () => (
            <Button
                text="BUTTON"
                theme="outline"
                color="red"
            />
        )
    )
    .add('green',
        () => (
            <Button
                text="BUTTON"
                color="green"
            />
        )
    )
    .add('red',
        () => (
            <Button
                text="BUTTON"
                color="red"
            />
        )
    )

    .add('default disabled',
        () => (
            <Button
                text="BUTTON"
                disabled={true}
            />
        )
    )
    .add('outline disabled',
        () => (
            <Button
                text="BUTTON"
                theme="outline"
                disabled={true}
            />
        )
    )
    .add('full width',
        () => (
            <Button
                text="BUTTON"
                fullWidth={true}
            />
        )
    )