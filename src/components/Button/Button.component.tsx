import React, { SFC } from 'react'
import { Button } from '@material-ui/core'
import { noop } from 'lodash' // Temporary
import './Button.component.scss'

type DefaultProps = Readonly<typeof defaultProps>

const defaultProps: IButton = {
  text: '',
  onClick() { noop() },
  type: 'button',
  size: 'medium',
  disabled: false,
  theme: 'default',
  fullWidth: false,
  color: undefined,
}

const ButtonComponent: SFC<IButton & DefaultProps> = (props) => {

  const {
    text,
    onClick,
    type,
    size,
    disabled,
    theme,
    fullWidth,
    color,
  } = props


  let containerClass = `button-container`
  containerClass = `${containerClass} ${type}`
  containerClass = `${containerClass} ${size}`
  containerClass = `${containerClass} ${disabled ? 'disabled' : ''}`
  containerClass = `${containerClass} ${theme}`
  containerClass = `${containerClass} ${fullWidth ? 'full-width' : ''}`
  containerClass = `${containerClass} ${color ? color : ''}`

  return (
    <div className={containerClass}>
      <Button
        type={type}
        className={containerClass}
        onClick={disabled ? noop : onClick}
        disabled={disabled}
      >
        <h6 className="link">{text}</h6>
      </Button>
    </div>
  )
}

ButtonComponent.defaultProps = defaultProps

export default ButtonComponent