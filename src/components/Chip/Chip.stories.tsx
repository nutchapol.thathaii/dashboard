import React from 'react';
import { storiesOf } from '@storybook/react';
import Chip from './Chip.component';

storiesOf('Chip', module)
    .add('Small default',
        () => (
            <Chip label={2} />
        )
    )
    .add('Small isHaveData',
        () => (
            <Chip label={3} size="small"/>
        )
    )
    .add('Medium default',
        () => (
            <Chip label={0} size="medium"/>
        )
    )
    .add('Medium isHaveData',
        () => (
            <Chip label={3} size="medium"/>
        )
    )