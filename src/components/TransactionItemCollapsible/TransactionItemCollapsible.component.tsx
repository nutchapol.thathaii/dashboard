import React, { FC, useState, Fragment } from 'react'
import { date, transformer } from 'utils'
import { split, groupBy, Dictionary, isEmpty, map, keys, get, capitalize, upperCase } from 'lodash'
import { LOTTO_GAME_TYPE_NAME, TRANSACTION_TYPE, LOTTO_SLUG_NAME } from 'constants/variables'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronRight } from '@fortawesome/free-solid-svg-icons'
import './transactionItemCollapsible.style.scss'
import colors from 'constants/colors'
import {
  Grid,
  TableRow,
  TableCell,
  // TableBody,
} from '@material-ui/core'

const constants = {
  round: 'รอบที่',
  makeLotto: 'แทง',
  waitResult: 'รอผลออก',
  actureResult: 'เลขที่ออก',
  game: 'เกมส์',
}

const statusName: { [status in TBetStatus | TFinanceStatus]: { name: string; color: string } } = {
  WAIT: { name: 'รอตรวจสอบ', color: colors.SECONDARY_GREEN },
  SUCCESS: { name: 'สำเร็จ', color: colors.SECONDARY_GREEN },
  FAIL: { name: 'ไม่สำเร็จ', color: colors.PRIMARY_RED },
  WINNER: { name: 'ได้', color: colors.SECONDARY_GREEN },
  LOSER: { name: 'เสีย', color: colors.PRIMARY_RED },
  CANCEL: { name: 'ยกเลิก', color: colors.SECONDARY_TEXT },
  DRAW:  { name: 'เสมอ', color: colors.SECONDARY_TEXT },
}

declare interface ITransactionItemCollapsible {
  credit: ICredit
}

type DefaultProps = Readonly<typeof defaultProps>

const defaultProps: ITransactionItemCollapsible = {
  credit: {
    money: 0,
    status: '',
    createdAt: '',
    groupType: '',
    slug: '',
    list: [],
  },
}

const getDisplayTransactionName = (groupType: string) => {
  const lendingType = get(groupType.split('_', 1), '0', '')
  const trailingType = groupType.replace(`${lendingType}_`, '')
  const subLendingType = get(trailingType.split('_', 1), '0', '')

  if (lendingType === 'FINANCE') {
    return TRANSACTION_TYPE[trailingType as TTransactionType]
  } else if (lendingType === 'BET') {
    if (subLendingType === 'GAME') {
      const trailingBetType = trailingType.replace(`${subLendingType}_`, '')
      const gameNameUnderscore = trailingBetType.replace(/CASINO_JOKER_|CASINO_THBGAME_/, '')
      const gameName = capitalize(upperCase(gameNameUnderscore))
      return `เดิมพันเกม ${gameName}`
    }
    // LOTTO type
    return LOTTO_SLUG_NAME[trailingType as TLottoSlug]
  }
}

const columns: ITransactionItemCollapsibleColumn<'lottoGameType' | 'betRate' | 'betResult'>[] = [
  { id: 'lottoGameType', label: '', minWidth: 150, align: 'center', borderBottom: 'unset' },
  { id: 'betRate', label: 'อัตราจ่าย(บาทละ)', minWidth: 200, align: 'right', borderBottom: 'unset' },
  { id: 'betResult', label: 'ยอดรวม', minWidth: 200, align: 'right', borderBottom: 'unset' },
];


const TransactionItemCollapsible: FC<ITransactionItemCollapsible & DefaultProps> = (props) => {

  const [isExpand, setExpand] = useState<boolean>(false)
  const { credit } = props

  const handleOnExpandClick = () => {
    setExpand(!isExpand)
  }

  const getGroupType = () => {
    const [nameSplited, typeSplited, subTypeSplited, endPoinTypeSplited] = split(credit.groupType, '_')
    return {
      name: nameSplited,
      type: typeSplited,
      subType: subTypeSplited || '',
      endPointType: endPoinTypeSplited || '',
    }
  }

  const getSlug = () => {
    const [slugName, slugType, slugRound] = split(credit.slug || '', '_')
    return {
      name: slugName,
      type: slugType,
      round: slugRound,
    }
  }

  const getStatus = () => {
    if (name === 'FINANCE') {
      if(type === 'DEPOSIT' && endPointType === 'FISHING') {
        return  { name: 'สำเร็จ', color: colors.PRIMARY_GREEN }
      }
      // withdraw
      else if(type === 'WITHDRAW' && endPointType !== 'FISHING') {
        return  { name: 'สำเร็จ', color: colors.PRIMARY_RED }
      }
      // game
      else if(type === 'WITHDRAW' && endPointType === 'FISHING') {
        return  { name: 'สำเร็จ', color: colors.PRIMARY_RED }
      }
      return statusName[credit.status as TFinanceStatus]
    } else if (name === 'BET') {
      if (credit.status === 'WAIT') {
        return { name: constants.makeLotto, color: statusName[credit.status as TBetStatus].color }
      }
      return statusName[credit.status as TBetStatus]
    }
    return { name: '', color: 'transparent' }
  }

  const { name, subType, type, endPointType } = getGroupType()
  const displayName = getDisplayTransactionName(credit.groupType)
  const displayTime = date.calibratingTime(credit.createdAt).format('DD MMM YYYY HH:mm')
  const round = (subType === 'YEGEE')
    ? ` (${constants.round} ${Number(getSlug().round)})`
    : ''

  const createData = (txData: ICreditDetail) => {
    return {
      lottoGameType: txData.numbers,
      betRate: get(txData, 'rate', '0'),
      betResult:  get(txData, 'money', '0'),
    };
  }

  const handleTextAlign = (align: any) => {
    let alignType
    switch (align) {
      case 'center':
        alignType = 'MuiTableCell-alignCenter'
        break
      case 'right':
        alignType = 'MuiTableCell-alignRight'
        break
      case 'left':
        alignType = 'MuiTableCell-alignLeft'
        break
    }
    return alignType
  }

  const CreditInfoListComponent = () => {
    if (name === 'FINANCE') {
      return map(credit.list, (cred, creditIndex) => {
        const stColor = get(statusName, `${cred.status}.color`, '')
        const stName = get(statusName, `${cred.status}.name`, '')
        return (
          <div
            className="d-flex flex-row align-items-center transaction-description-row"
            key={`transaction-description-amount-${creditIndex}`}
          >
            <div className="transaction-description-name-text">
              {TRANSACTION_TYPE[cred.type as TTransactionType]}
              {' '}
              <span className="transaction-description-lotto-number">{cred.numbers}</span>
              {' '}
              (<span style={{ color: stColor }}>{stName}</span>)
            </div>
            <div className="transaction-description-amount">{transformer.typeFormat(String(cred.money))}</div>
          </div>
        )
      })
    } else {
      // BET
      const creditGroupList: Dictionary<ICreditDetail[]> = groupBy<ICreditDetail>(credit.list, 'type')
      if (isEmpty(creditGroupList)) { return <></> }

      return map(keys(creditGroupList), (key, keyIndex) => {
        const creditDetails = creditGroupList[key]
        const DetailComponents = map(creditDetails, (detail, detailIndex) => {
          const stName = get(statusName, `${detail.status}.name`, '')
          const stColor = get(statusName, `${detail.status}.color`, '')
          const tableRow = createData(detail)
          return (
            <div
              className="d-flex flex-row align-items-center transaction-description-row"
              key={`transaction-description-detail-${detailIndex}`}
            >
              <TableRow>
                {columns.map((column) => {
                  const value = column.id !== 'lottoGameType'
                    ? transformer.typeFormat(tableRow[column.id])
                    : tableRow[column.id]
                  const valColor = column.id === 'betRate' ? colors.PRIMARY_PURPLE : colors.SECONDARY_TEXT
                  return (
                    <TableCell
                      key={column.id}
                      align={column.align}
                      className={handleTextAlign(column.align)}
                      style={{ minWidth: column.minWidth, borderBottom: 'unset' }}
                    >
                      {column.id === 'lottoGameType'
                        ?
                        (
                          <div style={{ color: valColor }}>{value}
                          <span style={{ color: stColor }}> {stName} </span></div>
                        )
                        : <div style={{ color: valColor }}>{value} </div>}

                    </TableCell>
                  );
                })}
              </TableRow>
            </div>
          )
        })

        return (
          <Fragment key={`transaction-description-amount-${keyIndex}`}>
            <div className="row mt-3">
              <div className="col">
                <TableRow>
                  {
                    columns.map((column) => (
                      <TableCell
                        key={column.id}
                        align={column.align}
                        style={{ minWidth: column.minWidth, borderBottom: 'unset' }}
                      >
                        {column.id === 'lottoGameType' ? LOTTO_GAME_TYPE_NAME[key as TLottoGameType] : column.label}
                      </TableCell>
                    ))
                  }
                </TableRow>
              </div>
            </div>
            {DetailComponents}
          </ Fragment>
        )
      })
    }
  }
  const trxColor = get(getStatus(), 'color', '')
  return (
    <Grid container>
      <Grid item xs={12}>
        <div className="py-3 credit-info-item-container" onClick={handleOnExpandClick}>
          <div className="col d-flex credit-info-item-wrapper">
            <div className="d-flex flex-column transaction-leading-wrapper">
              <div className="transaction-name-text d-flex align-items-center">
                {displayName}
              </div>
              <div className="transaction-time-text secondary-text py-2">{displayTime} {round}</div>
            </div>
            <div className="transaction-amount-text" style={{ color: trxColor }}>
              {transformer.typeFormat(String(credit.money))}
            </div>
            <div className="d-flex transaction-chevron-right-icon">
              <FontAwesomeIcon icon={faChevronRight} className={`chevron-right-icon ${isExpand ? 'expanded' : ''}`} />
            </div>
          </div>
          <div className={`transaction-description-container primary-dark ${isExpand ? 'expanded' : ''}`}>
            <div className="d-flex flex-column transaction-description-wrapper">
              {CreditInfoListComponent()}
            </div>
          </div>
        </div>
      </Grid>
    </Grid>
  )
}

TransactionItemCollapsible.defaultProps = defaultProps

export default TransactionItemCollapsible