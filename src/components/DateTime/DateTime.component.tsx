import React, { SFC } from 'react'
import { noop } from 'lodash'
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardDateTimePicker,
} from '@material-ui/pickers';
import './DateTime.component.scss'
import { th } from 'date-fns/locale';
type DefaultProps = Readonly<typeof defaultProps>

const defaultProps: IDateTimeProps = {
    id: '',
    label: '',
    value: null,
    disableToolbar: false,
    variant: 'inline',
    margin: 'normal',
    onClick() { noop() },
    onChange(date: Date | null) { noop() },
    fullWidth: false,
    disabled: false,
}


const DateTimeComponent: SFC<IDateTimeProps & DefaultProps> = (props) => {
    const {
        id,
        label,
        disableToolbar,
        variant,
        onClick,
        onChange,
        value,
        fullWidth,
        disabled,
    } = props

    return (
        <div className="text-field-container">
            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={th}>
                <KeyboardDateTimePicker
                    disablePast={true}
                    disabled={disabled}
                    id={id}
                    label={label}
                    disableToolbar={disableToolbar}
                    variant={variant}
                    inputVariant="outlined"
                    format="dd/MM/yyyy:HH:mm"
                    value={value}
                    onClick={onClick}
                    onChange={onChange}
                    fullWidth={fullWidth}
                    KeyboardButtonProps={{
                        'aria-label': 'change date',
                    }}
                />
            </MuiPickersUtilsProvider>
        </div>
    )
}
DateTimeComponent.defaultProps = defaultProps
export default DateTimeComponent