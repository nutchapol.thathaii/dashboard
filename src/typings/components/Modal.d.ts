declare interface IModalState {
  show: boolean
  extraProps: Object<any>
  RenderComponent: JSX.Element | Element
}

declare interface IModalShowProps {
  state: 'show' | 'update'
  extraProps?: Object<any>
  component: JSX.Element | Element
}

declare interface IModalHideProps {
  state: 'hide'
  extraProps?: Object<any>
  component?: JSX.Element | Element
}

declare interface ISuccessModal {
  title?: string
  description?: string
  actionText?: string
  action?(): void
}
declare interface IAcceptsModal {
  description?: string
  title?: string
  action?(): void
  cancelAction?(): void
  twoDown?: string
  threeUp?: string
  fourSuit?: string
}

declare interface ITransactionItemCollapsibleModal {
  userName?: string
  titleLabel?: string
  acceptsLabel?: string
  title?: string
  action?(): void
  cancelAction?(): void
  twoDown?: string
  threeUp?: string
  fourSuit?: string
  hasMore?: boolean
  ID?: string
  money?: string
  creditName?: string
  creditInfo?: ICredit[]
}

declare interface IErrorModal {
  title?: string
  description?: string
  actionText?: string
  action?(): void
}

declare interface IDepositSuccessModal {
  name?: string
  bank?: string
  accountNo?: string
  money?: string
  title?: string
  subTitle?: string
  description?: string
  cancel?: string
  approval?: string
  txStatus: ESTATUS
  txId: number
  action?(status: ESTATUS, txId: number): void
  cancelAction?(): void
}


declare interface IDepositErrorModal {
  name?: string
  bank?: string
  accountNo?: string
  money?: string
  title?: string
  subTitle?: string
  description?: string
  cancel?: string
  disApproval?: string
  txStatus: ESTATUS
  txId: number
  action?(description: string, status: ESTATUS, txId: number): void
  cancelAction?(): void
}

declare interface IWithDrawSuccessModal {
  name?: string
  bank?: string
  accountNo?: string
  money?: string
  title?: string
  subTitle?: string
  description?: string
  cancel?: string
  approval?: string
  webBankList?: IWebBank[]
  txStatus: ESTATUS
  txId: number
  action?(webBankId: number, status: ESTATUS, txId: number): void
  cancelAction?(): void
}


declare interface IWithDrawErrorModal {
  name?: string
  bank?: string
  accountNo?: string
  money?: string
  title?: string
  subTitle?: string
  description?: string
  cancel?: string
  disApproval?: string
  txStatus: ESTATUS
  txId: number
  action?(description: string, status: ESTATUS, txId: number): void
  cancelAction?(): void
}

