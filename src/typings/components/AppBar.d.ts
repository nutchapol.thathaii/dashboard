declare interface IAppBarComponentProps {
    userMeResult: any
    logout?(): void
    onChangeMenu?(data: IMenuList): void
    menu?: IMenu
}