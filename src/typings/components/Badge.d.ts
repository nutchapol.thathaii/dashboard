declare interface IBadge {
  text: string
  color?: string
  backgroundColor?: string
}