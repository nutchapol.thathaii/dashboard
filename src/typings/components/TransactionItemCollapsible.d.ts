declare interface ITransactionItemCollapsibleColumn<T = any> {
    id: T
    label: string;
    minWidth?: number;
    borderBottom?: string;
    align?: 'right' | 'center' | 'left';
    font?: JSX.Element
    format?: (value: number) => string;
  }

  declare interface ITransactionItem {
    lottoGameType: string
    betRate: string
    totalBet: string;
    betResult: string;
  }