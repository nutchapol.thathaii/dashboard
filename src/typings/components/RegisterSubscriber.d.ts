declare interface RegisterSubscriberState {
    dataA: ILinePoint[]
    dataB: ILinePoint[]
    dataC: ILinePoint[]
    formatTime: (date: Date) => string
    timer: NodeJS.Timeout | number | undefined
}