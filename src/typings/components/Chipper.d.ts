declare type IChipperColor = 'green' | 'red' | 'yellow' | 'default'

declare interface IChipperProps {
    label: string
    color?: IChipperColor
}