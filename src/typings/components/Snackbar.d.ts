declare type ISnackbarVariant = 'success' | 'info' | 'error' | 'warning'

declare interface ISnackbarShowProps {
    message: string
}

declare interface ISnackbarSubScribeProps {
    variant: ISnackbarVariant
    message: string
}