declare interface ITextField {
    id?: string
    error?: boolean
    helperText?: string
    name?: string
    label?: string
    type?: string
    placeholder?: string
    value?: string
    fullWidth?: boolean
    multiline?: boolean
    rows?: number
    InputLabelProps?: shrink
    startElement?: JSX.Element | undefined
    endElement?: JSX.Element | undefined
    maxLength?: number | undefined
    onClick?(e: FocusEvent<any>): void
    onBlur?(e: FocusEvent<any>): void
    onChange?(e: FocusEvent<any>): void
    disabled?: boolean
    betRate?: string | undefined
}

declare interface shrink {
    shrink: boolean
}