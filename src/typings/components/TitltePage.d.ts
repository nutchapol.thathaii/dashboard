interface ITitlePageProps {
    title: string
    className?: string
}