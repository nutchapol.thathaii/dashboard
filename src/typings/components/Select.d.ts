declare interface ISelect {
    id?: string
    error?: boolean
    helperText?: string
    name?: string
    label?: string
    type?: string
    placeholder?: string
    value?: string
    fullWidth?: boolean
    itemList?: ISelectItem[]
    isMultiline?: boolean
    startElement?: JSX.Element | undefined
    endElement?: JSX.Element | undefined
    onClick?(e: FocusEvent<any>): void
    onBlur?(e: FocusEvent<any>): void
    onChange?(e: FocusEvent<any>): void
}

declare interface ISelectItem {
    value: any
    element: JSX.Element
}