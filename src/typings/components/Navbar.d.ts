declare interface INavbarProps {
  mode?: 'light-mode' | 'dark-mode' | string
  onPressesLogo?(): void
}