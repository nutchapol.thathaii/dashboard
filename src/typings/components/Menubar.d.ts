declare interface IMenuBarProps {
  menu?: IMenu
  userMeIsFetching: boolean
  userMeError: string
  userMeCode: number | string
  userMeResult: any
}

declare interface IMenuBarComponentProps {
  notification: INotification
  userMeIsFetching: boolean
  userMeError: string
  userMeCode: number | string
  userMeResult: any
}

declare interface IMenuActionProps {
  onChangeMenu?(data: IMenuList): void
}


declare interface IMenuState {
  activeMenu: string
}