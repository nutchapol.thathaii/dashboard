type ButtonTheme = 'default' | 'outline'
type ButtonSize = 'small' | 'medium' | 'large'
type ButtonType = 'submit' | 'reset' | 'button'
type ButtonColor = 'green' | 'red' | 'yellow'

declare interface IButton {
  text: string | JSX.Element
  onClick?(): void
  type?: ButtonType
  size?: ButtonSize
  disabled?: boolean
  theme?: ButtonTheme
  fullWidth?: boolean
  color?: ButtonColor
}