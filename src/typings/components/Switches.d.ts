
declare type ISwitchesColor = 'primary' | 'secondary' | 'default'

declare interface ISwitches {
    color?: ISwitchesColor
    checked?: boolean
    onChange?(e: FocusEvent<any>): void
    name?: string
} 