declare type IWrapperVariant = 'dialog' | 'inline' | 'static';
declare type IMargin = 'none' | 'dense' | 'normal';

declare interface IDateTimeProps {
    id?: string
    label?: String
    disableToolbar?: boolean
    variant?: IWrapperVariant
    margin?: IMargin
    value?: Date | null | string
    onClick?(e: FocusEvent<any>): void
    onChange(date: Date | null ): void
    fullWidth?: boolean
    disabled?: boolean
}