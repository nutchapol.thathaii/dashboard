declare type IChipSize = 'small' | 'medium'
declare type IChipColor = 'green' | 'red' | 'yellow' | 'default' | 'gray'

declare interface IChipProps {
    label: number
    size?: IChipSize
    color?: IChipColor
} 