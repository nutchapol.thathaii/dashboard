declare interface IImageSet {
  [name: string]: {
    key: string
    name?: string
    Icon: string
  }
}

declare type IImageFlag = {
  [name in TFlag]: {
    name?: string
    Icon: string
  }
}