declare interface IWebConfigContainerState {
    textRunner: string
    contactUrl: string
    contactLine: string
    contactPhoneNumber: string
    updatedAt: string
}

declare interface IWebConfigContainerProps {
    getWebConfigIsFetching: boolean
    getWebConfigError: string
    getWebConfigCode: number | string
    getWebConfigResult: IWebConfig

    putWebConfigIsFetching: boolean
    putWebConfigError: string
    putWebConfigCode: number | string
    putWebConfigResult: any
}


declare interface IWebConfigContainerActionProps {
    getWebConfig(): void
    putWebConfig(data: IWebConfigPutData): void
    loader(data: boolean): void
}