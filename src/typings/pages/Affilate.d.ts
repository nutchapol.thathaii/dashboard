declare interface IAffilateContainerProps {
    getAffilateAllIsFetching: boolean
    getAffilateAllError: string
    getAffilateAllCode: number | string
    getAffilateAllResult: IAffilate

    putAffilateIsFetching: boolean
    putAffilateError: string
    putAffilateCode: number | string
    putAffilateResult: any
}

declare interface IAffilateContainerState {
    affLotterRate: string,
    affGameRate: string,
    affBallRate: string,
    affSystemRate: string,
}

declare interface ITransformObjectNum2Percentage {
    [propName: string]: string
  }

declare interface IAffilateContainerActionProps {
    getAffilateAll(): void
    putAffilate(data: IAffilatePutData): void
    loader(data: boolean): void
}

declare interface IAffilate {
    lotterRate: string
    gameRate: string
    ballRate: string
    systemRate: string
}