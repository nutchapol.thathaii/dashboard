declare interface ILinePoint {
    x: Date
    y: number
}

declare interface IIncomeSummaryState<T> {
    plotData: T[]
}

declare interface IIncomeSummaryRangeOfData {
    max: number
    min: number
}

declare interface ILotterNumberSessionComponentProps {
    lotterNumberSessionList: ILotterNumberSession[]
}

declare interface ILotterNumberSessionTransactionRow {
    number: string
    totalBet: string
    result: string
    rate: string
    rateLevel: string
}

declare interface IResponseReportOverview {
    allIncome: number
    monthIncome: number
    monthBetTxMaxIncome: number
    monthBetTxMinIncome: number
    dayBetTxMaxIncome: number;
    dayOfBetTxMaxIncome: string;
    dayBetTxMinIncome: number;
    dayOfBetTxMinIncome: string;
    incomeLine: {
        x: string
        y: number | null
    }[]
    moneyLoseLine: {
        x: string
        y: number | null
    }[]
}

declare interface IResponseReportLotterNumberSession {
    dataList: ILotterNumberSession[]
    sumBetTotal: number
}


declare interface IResponseReportYegee {
    allIncome: number;
    averageIncome: number;
    totalUser: number;
    averageUser: number;
    dataList: { 'ได้': number; 'เสีย': number; round: number}[]
}

declare interface IResponseReportDepositWithdraw {
    dataList: IReportDepositWithdrawList[]
    balance: number
    graphType: 'horizontal' | 'vertical'
}

declare interface IDashboardState {
    queryIncomeQueryMonth: string
    queryLotterDepositWithdraw: string
    queryYegeeDate: string
    queryLotterNumberSessionType: string
    queryLotterNumberSessionCode: string
    incomeSystem: IResponseReportOverview
    yegeeDaily: IResponseReportYegee
    daysInMonth: { value: string; element: JSX.Element }[]
    DepositWithdrawSummary: IResponseReportDepositWithdraw
}

declare interface IDashboardProps {
    getReportOverviewCode: number
    getReportOverviewError: string
    getReportOverviewIsFetching: boolean
    getReportOverviewResult: IResponseReportOverview

    getReportYegeeCode: number
    getReportYegeeError: string
    getReportYegeeIsFetching: boolean
    getReportYegeeResult: IResponseReportYegee

    getReportLotterNumberSessionCode: number
    getReportLotterNumberSessionError: string
    getReportLotterNumberSessionIsFetching: boolean
    getReportLotterNumberSessionResult: IResponseReportLotterNumberSession

    postReportDepositWithdrawCode: number
    postReportDepositWithdrawError: string
    postReportDepositWithdrawIsFetching: boolean
    postReportDepositWithdrawResult: IResponseReportDepositWithdraw
}

declare interface IDashboardActionProps {
    getReportOverview(data: IRequestReportOverview): void
    getReportYegee(data: IRequestReportYegee): void
    getReportLotterNumberSession(data: IRequestReportLotterNumberSession): void
    postReportLotterDepositWithdraw(data: IRequestReportDepositWithdraw): void
}