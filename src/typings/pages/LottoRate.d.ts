declare interface ILottoRateProps {
    getLottoScheduleIsFetching: boolean
    getLottoScheduleCode: number | string
    getLottoScheduleError: string
    lottoSchedule: ILottoSchedule[]

    getLottoRateLevelIsFetching: boolean
    getLottoRateLevelError: string
    getLottoRateLevelCode: number | string
    getLottoRateLevelResult: ILottoRateLevel[]

    postLottoRateLevelIsFetching: boolean
    postLottoRateLevelError: string
    postLottoRateLevelCode: number | string
    postLottoRateLevelResult: ILottoRateLevel[]

    deleteLottoRateLevelIsFetching: boolean
    deleteLottoRateLevelError: string
    deleteLottoRateLevelCode: number | string
    deleteLottoRateLevelResult: ILottoRateLevel[]

    postCheckRateClientIsFetching: boolean
    postCheckRateClientError: string
    postCheckRateClientCode: number | string
    postCheckRateClientResult: any
}

declare interface ILottoRateActionProps {
    loader(state: boolean): void
    getLottoSchedule(): void
    getLottoRateLevel(data: IGetLottoRateLevelParams): void
    postLottoRateLevel(data: ILottoCreateRateLevelData): void
    deleteLottoRateLevel(data: ILottoDeleteRateLevelData): void
    postCheckRateClient(data: IPostCheckRateClient): void
}

declare interface ILottoRateContainerState {
    id: number
    selectedDate: Date | null
    search: string
    onSettingLotto: TLottoSlug
    title: string
    startTime: string
    endTime: string
    lottoTypeSelect: TLottoGameType
    tableRows: ILottoRateLevelRow[]
    level: string
    rate: string
    volume_limit: string
    lottoSchedule: ILottoSchedule[]
    initialFormValue: ILottoRate
    icon: string
    textSubmit: string
    indexSelectRow: number
}

declare interface ILottoRateLevelRow {
    id: number
    level: string;
    rate: string;
    volume_limit: string;
}

declare interface ILottoRateActionButton {
    name?: string
    text?: string
    color?: ButtonColor
    type?: ButtonType
    status?: TGameStatus
    onSettingLotto?: string
}