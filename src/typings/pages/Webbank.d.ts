declare interface IWebBankContainerState {
    queryStatus: IQueryWebBankStatus
    webBankList: IWebBank[]
    webBankActive: IWebBank[]
    limit: number
    page: number
    total: number
    search: string
    queryTimeout: NodeJS.Timeout
}

declare interface IWebBankProps {
    postWebBankIsFetching: boolean
    postWebBankError: string
    postWebBankCode: number | string
    postWebBankResult: IWebBank[]

    getWebBankIsFetching: boolean
    getWebBankError: string
    getWebBankCode: number | string
    getWebBankResult: IWebBank[]
    getWebBankLimit: number
    getWebBankPage: number
    getWebBankTotal: number

    getWebBankActiveIsFetching: boolean
    getWebBankActiveError: string
    getWebBankActiveCode: number | string
    getWebBankActiveResult: PaginationResponse<IWebBank>

    putWebBankIsFetching: boolean
    putWebBankError: string
    putWebBankCode: number | string
    putWebBankResult: IWebBank[]

    deleteWebBankIsFetching: boolean
    deleteWebBankError: string
    deleteWebBankCode: number | string
    deleteWebBankResult: IWebBank[]


    getWebBankRateIsFetching: boolean
    getWebBankRateIsError: string
    getWebBankRateCode: number | string
    getWebBankRateResult: IRate
}

declare interface IWebBankActionProps {
    getWebBank(data: IGetWebBankParams): void
    getWebBankActive(): void
    getWebBankRate(): void
    putWebBank(data: IPutWebbankStoreState): void
    deleteWebBank(data: IDeleteWebbankStoreState): void
    loader(data: boolean): void
    listenWebbankSocket(data: IGetWebBankParams):void
    unlistenWebbankSocket() : void
}

declare interface IDeleteWebBankActionProps {
    deleteWebBank(data: IDeleteWebbankStoreState): void
    loader(data: boolean): void
}

declare interface ICreateWebBankActionProps {
    postWebBank(data: IPostWebbankStoreState): void
    loader(data: boolean): void
}

declare interface IPutWebBankActionProps {
    putWebBank(data: IPutWebbankStoreState): void
    loader(data: boolean): void
}

declare interface ICreateWebBankState {
    name: string
    accountNo: string
    type: string
}

declare interface IEditWebBankState {
    name: string
    accountNo: string
    type: string
    bank: string
}

declare interface IWebBankDetailColumn {
    id: 'id'|'bank' | 'accountNo' | 'user' | 'income'
    label: string;
    minWidth?: number;
    align?: 'right' | 'center' | 'left';
  }