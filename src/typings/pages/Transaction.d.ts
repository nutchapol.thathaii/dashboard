declare interface ITransactionProps {
  getTransactionIsFetching: boolean
  getTransactionError: string
  getTransactionCode: number | string
  getTransactionResult: IWalletTransactionPopuate[]
  getTransactionLimit: number
  getTransactionPage: number
  getTransactionTotal: number

  putTransactionIsFetching: boolean
  putTransactionError: string
  putTransactionCode: number | string
  putTransactionResult: ITransactionData[]

  postReserveTransactionIsFetching: boolean
  postReserveTransactionError: string
  postReserveTransactionCode: number | string
  postReserveTransactionResult: IWalletTransactionPopuate[]

  getWebBankActiveIsFetching: boolean
  getWebBankActiveError: string
  getWebBankActiveCode: number | string
  getWebBankActiveResult: PaginationResponse<IWebBank>

  getCreditInfoListCode: number | string
  getCreditInfoListError: string
  getCreditInfoListIsFetching: boolean
  creditInfoPagination: APIPagination<ICredit>
  creditInfo: ICredit[]
}

declare interface ITransactionActionProps {
  getTransaction(data: IGetTransactionParams): void
  putTransaction(data: ITransactionPutData): void
  getWebBankActive(): void
  postReserveTransaction(data: IWalletTransactionReserve): void
  forceCancelReserveTransaction (data: IWalletTransactionForceCancelReserve): void
  cancelReserveTransaction(data: IWalletTransactionReserve): void
  connectFinanceDepositSocket(data: IGetTransactionParams): void
  connectFinanceWithdrawSocket(data: IGetTransactionParams): void
  unlistenTransactionAllSocket(): void
  loader(data: boolean): void
  getCreditInfoList(query: IGetCreditParams, isFirst?: boolean): void
  clearTransactionAll(): void
  clearCreditInfoList(): void
}


declare interface ITransactionContainerState {
  tableRows: ITransactionRow[]
  dataType: IWalletTransactionType
  dataStatus: IWalletTransactionStatus
  limit: number
  page: number
  total: number
  date: string
  search: string
  queryTimeout: NodeJS.Timeout
  dateTimeout: NodeJS.Timeout
  selectedDate: Date | null
  isShowSnack: boolean
  hasMore: boolean
  creditName: string
  isFirst: boolean
  getCreditInfo:IGetCreditInfo
}


interface IGetCreditInfo {
  ID: string
  isFirst: boolean
  money: string
}

declare interface ITransactionColumn<T = any> {
  id: T
  label: string;
  minWidth?: number;
  align?: 'right' | 'center' | 'left';
  font?: JSX.Element
  format?: (value: number) => string;
}

declare interface ITransactionRow {
  id: string;
  type: string;
  money: string;
  userName: string;
  userBank: string;
  userTxTime: string;
  bankName: string;
  bankAccountNo: string;
  walletBank: string;
  walletBankNo: string;
  status: JSX.Element;
  approveUserId: string;
  statement?: JSX.Element
  
  // action: JSX.Element
}

declare interface ITransactionData {
  id: number;
  tag: string
  type: string;
  money: string;
  userId: ITransactionDataUser;
  userTxTime: string;
  approveUserId: ITransactionDataUser
  status: string;
  pay_slip_image: string
  description: string
  userBankId: ITransactionDataUserBank
  webBankId: ITransactionDataWebBank
  bankAccountNo: ITransactionDataWebBank
  walletBank: ITransactionDataWebBank
  walletId: number
  created_at: string
  updated_at: string
}

declare interface IWalletTransactionPopuate {
  id: number;
  tag: string
  description: string
  money: string;
  status: string;
  type: string;
  userTxTime: string;
  approveUserId: ITransactionDataUser
  webBankId: ITransactionDataWebBank
  userBankId: ITransactionDataUserBank
  userId: ITransactionDataUser
  walletBank: ITransactionDataWebBank
  bankAccountNo: ITransactionDataWebBank
  walletId: IWallet
}
declare interface IWallet {
  id: number
  money: string
  created_at: string

}

declare interface ITransactionDataUser {
  username: string
  id: string
}
declare interface ITransactionDataUserBank {
  name: string
  type: string
  number: string
}
declare interface ITransactionDataWebBank {
  name: string
  number: string
  type: string
}