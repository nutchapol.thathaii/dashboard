
declare interface ILottoListProps {
    getLottoScheduleIsFetching: boolean
    getLottoScheduleCode: number | string
    getLottoScheduleError: string
    lottoSchedule: ILottoSchedule[]
    getLottoIsFetching: boolean
    getLottoError: string
    getLottoCode: number | string
    lottoList: ILotto[]
    putLottoModeIsFetching: boolean
    putLottoModeCode: number | string
    putLottoModeError: string
    putlottoModeResult: ILottoSchedule[]
    putLottoStatusIsFetching: boolean
    putLottoStatusCode: number | string
    putLottoStatusError: string
    putlottoStatusResult: ILottoSchedule[]
    postLottoRefundIsFetching: boolean
    postLottoRefundCode: number | string
    postLottoRefundError: string
    postlottoRefundResult: any
    postLottoResultIsFetching: boolean
    postLottoResultCode: number | string
    postLottoResultError: string
    postlottoResultResult: ILottoResult[]
    postLottoCalculateIsFetching: boolean
    postLottoCalculateCode: number | string
    postLottoCalculateError: string
    postlottoCalculateResult: any
}

declare interface ILottoListActionProps {
    loader(state: boolean): void
    getLottoSchedule(): void
    getLottoList(): void
    putLottoMode(data: ILottoModePutData): void
    putLottoStatus(data: ILottoStatusPutData): void
    postLottoRefund(data: ILottoRefundPostData): void
    postLottoResult(data: ILottoResultPostData): void
    postLottoCalculate(data: ILottoCalculatePostData): void
}

declare interface ILottoContainerState {
    id: number
    selectedDateStart: string
    selectedDateEnd: string
    search: string
    onSettingLotto: TLottoSlug
    title: string
    startTime: string
    endTime: string
    buttonControl: string
    checked: boolean
    dateTimeout: NodeJS.Timeout
    status: TGameStatus
    initialFormValue: ILottoDigits
    onSubmit: boolean
    onRefund: boolean,
    onCalculate: boolean,
    icon: string
  }

declare interface ILottoListActionButton {
    buttonControl?: string
    text: string
    color?: ButtonColor
    type?: ButtonType
    status?: TGameStatus
    onSettingLotto?: string
}