declare interface IContentProps {
}

declare interface IContentActionProps {
  loader(data: boolean): void
}


declare interface IContentContainerState {
  tableRows: IContentRow[]
  dataType: IWalletTransactionType
  dataStatus: IWalletTransactionStatus
  limit: number
  page: number
  total: number
  date: string
  search: string
  queryTimeout: NodeJS.Timeout
  dateTimeout: NodeJS.Timeout
  selectedDate: Date | null
  isShowSnack: boolean
}

declare interface IContentRow {
  id: string
  userBank: string;
  userName: string;
  bankName: string;
  set: JSX.Element;
}

declare interface IContentPopuate {
  id: string
  userBank: string;
  bankName: string;
  set: string
  userName: string
}