declare interface IReportLottoRow {
    id: string
    betNumber: string
    statusBet: string
    moneyIncome: string
    moneyOutcome: string
  }
  
  declare interface IReportLottoContainerState {
    tableRows: IReportLottoRow[]
    dataType: IWalletTransactionType
    dataStatus: IWalletTransactionStatus
    limit: number
    page: number
    total: number
    date: string
    search: string
    queryTimeout: NodeJS.Timeout
    dateTimeout: NodeJS.Timeout
    selectedDate: Date | null
    isShowSnack: boolean
    open: boolean
  }

  declare interface IReportLottoPopuate {
    id: string
    betNumber: string;
    statusBet: string;
    moneyIncome: string
    moneyOutcome: string
  }