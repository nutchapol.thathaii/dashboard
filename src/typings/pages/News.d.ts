declare interface INewsContainerProps {
    getNewsAllIsFetching: boolean
    getNewsAllError: string
    getNewsAllCode: number | string
    getNewsAllResult: INews[]

    postNewsIsFetching: boolean
    postNewsError: string
    postNewsCode: number | string
    postNewsResult: any

    putNewsIsFetching: boolean
    putNewsError: string
    putNewsCode: number | string
    putNewsResult: any

    deleteNewsIsFetching: boolean
    deleteNewsError: string
    deleteNewsCode: number | string
    deleteNewsResult: any
}

declare interface INewsContainerState {
    newsList: INews[]
    onEdit: boolean
    editTitle: string
    editDescription: string
    insertTitle: string
    insertDescription: string

    setselectedPar: null,
    lat: number,
    lng: number,
    thaiName: string,
    parArray: any,
    flagClose: boolean,
    id: string,
    sector: any
    north: boolean
    northeast: boolean
    central: boolean
    south: boolean
}

declare interface INewsContainerActionProps {
    getNewsAll(): void
    postNews(data: INewsPostData): void
    putNews(data: INewsPutData): void
    deleteNews(data: INewsDeleteData): void
    loader(data: boolean): void
}

declare interface INews {
    id: number
    title: string
    description: string
    createAt: string
    updatedAt: string
    isEdit?: boolean
}