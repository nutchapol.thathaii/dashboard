declare interface IRootProps {
  token: string
  loginIsFetching: boolean
  loginError: string
  loginCode: number | string
  loginResult: any
  userMeIsFetching: boolean
  userMeError: string
  userMeCode: number | string
  userMeResult: any
  isPersist: boolean
  getNotificationTransactionAllIsFetching: boolean
  getNotificationTransactionAllError: string
  getNotificationTransactionAllCode: number | string
  getNotificationTransactionAllResult: INotification
}

declare interface IRootStates {
  themeMode: 'dark-mode' | 'light-mode' | string
  consoleClassName: string
  permission: string
}

declare interface IRootActionProps {
  loader(state: boolean): void
  getUserMe(): void
  getNotificationTransactionAll(): void
  listenNotificationTransactionAllSocket(): void
  unlistenNotificationTransactionAllSocket(): void
  logout(): void
  clearUser(): void
  clearMenu(): void
}
declare interface IAppBarStates {
  themeMode: 'dark-mode' | 'light-mode' | string;
  hideMenu: boolean;
  consoleClassName: string;
  appBarClassName: string;
  menuBarClassName: string;
}
