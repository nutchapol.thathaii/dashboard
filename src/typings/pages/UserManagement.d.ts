declare interface IUserManagementProps {
    getUserAllIsFetching: boolean
    getUserAllError: string
    getUserAllCode: number | string
    getUserAllResult: IUserPopulate[]
    getUserAllLimit: number
    getUserAllPage: number
    getUserAllTotal: number
    
}

declare interface IUserCreateManagementProps {
    postUserIsFetching: boolean
    postUserError: string
    postUserCode: number | string
    postUserResult: IUser
    postUserdevMessage: string
}

declare interface IUserDeleteManagementProps {
    deleteUserIsFetching: boolean
    deleteUserError: string
    deleteUserCode: number | string
    deleteUserResult: IUser[]
}

declare interface IUserPutManagementProps {
    putUserIsFetching: boolean
    putUserError: string
    putUserCode: number | string
    putUserResult: IUser[]
}

declare interface IUserManagementActionProps {
    getUserAll(data: IGetUserParams): void
    loader(data: boolean): void
}

declare interface ICreateManagementActionProps {
    postUser(data: IManagementPostData): void
    loader(data: boolean): void
}

declare interface IDeleteManagementActionProps {
    deleteUser(data: IManagementDeleteData): void
    loader(data: boolean): void
}

declare interface IPutManagementActionProps {
    putUser(data: IManagementPutData): void
    loader(data: boolean): void
}

declare interface IUserManagementContainerState {
    limit: number
    page: number
    total: number
    search: string
    permission: string
    queryTimer: NodeJS.Timeout
}


declare interface IUserManagementContainerTableRow {
    id: string
    username: string
    phoneNumber: string
    userBank: JSX.Element
    createdAt: string
    action: JSX.Element
    delete: JSX.Element
}

declare interface IUserManagementCreateContainerState {
    bank: string
}
