declare interface ISettingsProps {

}
declare interface IHourListItem {
    value: any
    element: JSX.Element
}

declare interface IMinutesList {
    value: any
    element: JSX.Element
}

declare interface ISettingsState {
    idLotterYegeeRunDownRates: number
    idLotterYegeeThreeUpRates: number
    idLotterYegeeThreeToastRates: number
    idLotterYegeeTwoDownRates: number
    idLotterYegeeTwoUpRates: number
    idLotterYegeeRunUpRates: number

    lotterYegeeRunDownRates: number
    lotterYegeeThreeUpRates: number
    lotterYegeeThreeToastRates: number
    lotterYegeeTwoDownRates: number
    lotterYegeeTwoUpRates: number
    lotterYegeeRunUpRates: number
    hourList: IHourListItem[]
    minutesList: IMinutesList[]
    yegeeStartHour: string
    yegeeStartMinutes: string
    yegeeRound: string
    yegeeRoundTime: string
    expectRate: string
}

declare interface IBetRatesProps {
    getBetRatesAllIsFetching: boolean
    getBetRatesAllError: string
    getBetRatesAllCode: number | string
    getBetRatesAllResult: IBetRatesAll[]

    putBetRatesIsFetching: boolean
    putBetRatesError: string
    putBetRatesCode: number | string
    putBetRatesResult: IBetRatesAll[]

    getYegeeGameConfigAllIsFetching: boolean
    getYegeeGameConfigAllError: string
    getYegeeGameConfigAllCode: number | string
    getYegeeGameConfigAllResult: IYegeeConfigAll

    putYegeeGameConfigIsFetching: boolean
    putYegeeGameConfigError: string
    putYegeeGameConfigCode: number | string
    putYegeeGameConfigResult: IYegeeConfigAll

    getBetRatesIsFetching: boolean
    getBetRatesError: string
    getBetRatesCode: number | string
    getBetRatesResult: IBetRate[]
}

declare interface ISettingsActionProps {
    getBetRatesAll(): void
    putBetRates(data: IBetRatesPutData[]): void
    getYegeeGameConfigAll(): void
    putYegeeGameConfig(data: IPutYegeeConfigState): void
    loader(data: boolean): void
    getBetRate(data: IGetBetRateParams): void
}