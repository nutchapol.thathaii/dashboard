declare interface IHomeProps {
  loginIsFetching: boolean
  loginError: string
  loginCode: number | string
  loginResult: any
}

declare interface IHomeActionProps {
  login(data: ILoginRequest): void
  loader(data: boolean): void
}

declare interface ILoginFormProps {
  onNavigateToForgotPassword?(): viod
}