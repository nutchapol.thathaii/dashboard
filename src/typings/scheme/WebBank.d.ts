declare interface IEditWebBank {
    bank: string
    name: string
    number: string
  }

declare interface ICreateWebBank {
  bank: string
  number: string
}