declare interface ILottoDigits {
    twoDown: string
    threeUp: string
    fourSuit: string
    timeStamp?: number
}
