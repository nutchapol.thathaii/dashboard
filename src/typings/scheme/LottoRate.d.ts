declare interface ILottoRate {
    level: string
    rate: string
    volumeLimit: string
    isSecondButton?: boolean
    timeStamp?: number
}
