declare interface IRegister {
  phoneNumber: string
  username: string
  password: string
  confirmPassword: string
  affilateNumber: string
  bankNumber: string
  name: string
  number: string
}

declare interface IEditUser {
  username: string
  password: string
}