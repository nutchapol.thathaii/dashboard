declare interface ILottoRefundState {
  isFetching?: boolean
  data?: any
  error?: string
  code?: number | string
}

declare interface ILottoCalculateState {
  isFetching?: boolean
  data?: any
  error?: string
  code?: number | string
}

declare interface IConfigLottoState {
    schedule: ReducerState<ILottoSchedule[]>
    mode: ReducerState<ILottoSchedule[]>
    status: ReducerState<ILottoSchedule[]>
    refund: ILottoRefundState
    result: ReducerState<ILottoResult[]>
    list: ReducerState<ILotto[]>
    calculate: ILottoCalculateState
  }


declare interface ILottoRateState {
  list: ReducerState<ILottoRateLevel[]>
  create: ReducerState<ILottoRateLevel[]>
  remove: ReducerState<ILottoRateLevel[]>
}

declare interface IConfig {
    lotto: IConfigLottoState
    lottoRate: ILottoRateState
}