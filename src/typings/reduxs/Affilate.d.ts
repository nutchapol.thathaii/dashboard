declare interface IAffilateAllState {
    isFetching?: boolean
    data?: any
    error?: string
    code?: number | string
}

declare interface IAffilatePutData {
    lotter_rate: string
    game_rate: string
    ball_rate: string
    system_rate: string
}

declare interface IAffilateState {
    all: IAffilateAllState
    put: IAffilateAllState
}