declare interface IBetState {
    rate: ReducerState<IBetRate[]>
}

declare interface ILottoState {
    bet: IBetState
  }
