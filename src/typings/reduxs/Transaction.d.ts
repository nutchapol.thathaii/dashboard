declare interface ITransactionAllState {
  isFetching?: boolean
  data?: any
  error?: string
  code?: number | string
  total?: number
  page?: number
  limit?: number
}

declare interface INotificationAllState {
  isFetching?: boolean
  data?: any
  error?: string
  code?: number | string
}

declare interface ITransactionPutState {
  isFetching?: boolean
  data?: any
  error?: string
  code?: number | string
}

declare interface IReserveTransactionPostState {
  isFetching?: boolean
  data?: any
  error?: string
  code?: number | string
}

declare interface IReserveTransactionDeleteState {
  isFetching?: boolean
  data?: any
  error?: string
  code?: number | string
}

declare interface ITransactionState {
  all: ITransactionAllState
  put: ITransactionPutState
  reserve: IReserveTransactionPostState
  reserveDelete: IReserveTransactionDeleteState
  notification: INotificationAllState
}