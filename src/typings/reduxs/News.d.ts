declare interface INewsAllState {
    isFetching?: boolean
    data?: any
    error?: string
    code?: number | string
}

declare interface INewsPostData {
    title: string
    description: string
}

declare interface INewsPutData {
    id: number
    title: string
    description: string
}

declare interface INewsDeleteData {
    id: number
}

declare interface INewsState {
    all: INewsAllState
    post: INewsAllState
    put: INewsAllState
    delete: INewsAllState
}