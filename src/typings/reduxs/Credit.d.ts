declare interface ICreditInfoListState extends ReducerState<ICredit[]> {
  pagination?: APIPagination<ICredit>
}

declare interface ICreditState {
  list: ICreditInfoListState
}

declare interface IGetCreditParams {
  page: number
  limit: number
  id: string
  money: string
}