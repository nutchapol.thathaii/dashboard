declare interface IUserToken {
  isPersist: boolean
  accessToken?: string
  refreshToken?: string
}

declare interface IUserState {
  token: IUserToken
  userMe: IGetMeState
  userAll: IGetUserAllState
  userCreate: IUserPostState
  userEdit: IUserPutState
  userDelete: IUserDeleteState
}

declare interface IGetMeState {
  isFetching?: boolean
  data?: any
  error?: string
  code?: number | string
}

declare interface IUserPostState {
  isFetching?: boolean
  data?: any
  error?: string
  code?: number | string
  devMessage?: string
}

declare interface IUserPutState {
  isFetching?: boolean
  data?: any
  error?: string
  code?: number | string
}

declare interface IUserDeleteState {
  isFetching?: boolean
  data?: any
  error?: string
  code?: number | string
}
declare interface IGetUserAllState {
  isFetching?: boolean
  data?: any
  error?: string
  code?: number | string
  total?: number
  page?: number
  limit?: number
}

declare interface IUser {
  id: number;
  username: string;
  password: string;
  phoneNumber: string;
  walletId: number;
  userBankId: number;
  affilateMeUuid: string;
  permission: string;
  isFake: boolean;
  createdAt: string;
  updatedAt: string;
  bank?: Bank;
  wallet?: Wallet;
  action?: any
}

declare interface IUserPopulate {
  id: number;
  username: string;
  password: string;
  phoneNumber: string;
  userBank: Bank;
  affilateMeUuid: string;
  permission: string;
  isFake: boolean;
  createdAt: string;
  updatedAt: string;
  wallet: Wallet;
  action?: any
}

declare interface Bank {
  id: number
  type: string
  name: string
  number: string
  isFake: boolean
  createdAt: string
  updatedAt: string
}

declare interface IGetUserParams {
  permission: string
  search: string
  page: number
  limit: number
}