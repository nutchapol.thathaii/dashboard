declare type IMenuList =
  'deposit' |
  'webbank' |
  'settings' |
  'withdraw' |
  'news' |
  'web-config' |
  'summary-dashboard' |
  'members' |
  'user-management' |
  'admin-management' |
  'staff-management' |
  'lotto' |
  'lotto-rate' |
  'content' |
  'lotter-master-management' |
  'report-yegee' |
  '/'

declare interface IMenu {
  activeControl: IMenuActiveControl
}

declare interface IMenuActiveControl {
  menu: IMenuList
}
