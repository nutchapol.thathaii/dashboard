declare interface IBetRatesAllState {
  isFetching?: boolean
  data?: any
  error?: string
  code?: number | string
}

declare interface IPutBetRatesState {
  isFetching?: boolean
  data?: any
  error?: string
  code?: number | string
}

declare interface IBetRatesPutData {
  id_bet: number
  rate: string
}

declare interface IBetRatesState {
  allBetRates: IBetRatesAllState
  putBetRate: IPutBetRatesState
}