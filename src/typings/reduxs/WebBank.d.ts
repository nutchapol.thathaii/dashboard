declare interface IWebbankStoreState {
  isFetching?: boolean
  data?: any
  error?: string
  code?: number | string
  total?: number
  page?: number
  limit?: number
}

declare type IQueryWebBankStatus = 'ACTIVE' | 'INACTIVE' | 'READY_TO_HARVEST'

declare interface IGetWebBankParams {
  queryStatus?: IQueryWebBankStatus
  page?: number
  limit?: number
  search?: string
}

declare interface IPostWebbankStoreState {
  type: string
  name: string
  number: string
}

declare interface IPutWebbankStoreState {
  id: number
  name: string
  number: string
}

declare interface IDeleteWebbankStoreState {
  id: number
}

declare interface IWebbanksState {
  postWebbank:IWebbankStoreState
  getWebbank: IWebbankStoreState
  getWebbankActive: IWebbankStoreState
  putWebbank: IWebbankStoreState
  deleteWebbank: IWebbankStoreState
}

declare interface IWebBankRateState {
  get: ReducerState<IRate>
}

declare interface IWebBankState {
  webbanks: IWebbanksState
  rate: IWebBankRateState
}
