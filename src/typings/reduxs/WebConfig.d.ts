declare interface IWebConfigState {
    isFetching?: boolean
    data?: any
    error?: string
    code?: number | string
}

declare interface IWebConfigPutData {
    text_runner: string
    contact_url: string
    contact_line: string
    contact_phone_number: string
}

declare interface IWebConfigReduxState {
    get: IWebConfigState
    put: IWebConfigState
}