declare interface IPaginationStore {
    page?: number
    limit?: number
    total?: number
}