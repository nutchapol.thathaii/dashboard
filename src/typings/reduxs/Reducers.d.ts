declare interface ReducerState<T = any> {
  isFetching?: boolean
  data?: T
  error?: string
  code?: number
}

declare interface RootReducers {
  mantra: {
    user: IUserState
    loader: boolean
    auth: IAuthState
    transaction: ITransactionState
    settings: IBetRatesState & IYegeeState
    webbank: IWebBankState
    menu: IMenu
    news: INewsState
    webConfig: IWebConfigReduxState
    socket: ISocketState
    affilate: IAffilateState
    config: IConfig
    lotto: ILottoState
    check: IRateClientState
    report: IReportState
    credit: ICreditState
  }
}