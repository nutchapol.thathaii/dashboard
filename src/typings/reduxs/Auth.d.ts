
declare interface ILoginData {
  token: string
}

declare type IPermisson = 'USER' | 'STAFF' | 'ADMIN' | 'SUPER_ADMIN'
declare interface ITokenData {
  permission: IPermisson
  exp: number
  iat: number
  secret_key: string
  user_id: number
}

declare interface ILoginState {
  isFetching?: boolean
  data?: ILoginData
  error?: string
  code?: number | string
}

declare interface IAuthState {
  login: ILoginState
}