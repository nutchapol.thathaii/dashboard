declare interface IRequestReportOverview {
    type: 'day' | 'month' | 'year'
    date: string
}

declare interface IReportOverview<T> {
    allIncome: number
    monthIncome: number
    monthBetTxMaxIncome: number
    monthBetTxMinIncome: number
    incomeLine: T[]
    moneyLoseLine: T[]
}

declare interface IReportOverviewReduxState {
    isFetching?: boolean
    code?: any
    data?: any
    error?: string
}
declare interface IRequestReportYegee {
    date: string
}

declare interface IReportYegee {
    allIncome: number;
    averageIncome: number;
    totalUser: number;
    averageUser: number;
    dataList: { 'ได้': number; 'เสีย': number; round: number }[]
}

declare interface IReportYegeeReduxState {
    isFetching?: boolean
    code?: any
    data?: any
    error?: string
}


declare interface IRequestReportLotterNumberSession {
    type: string
    code: string
}

declare interface IReportLotterNumberSession {
    dataList: ILotterNumberSession[]
    sumBetTotal: number
}

declare interface ILotterNumberSession {
    number: string;
    totalBet: number;
    result: number;
    rateLevel: IRateLevel;
}

declare interface IRateLevel {
    id: number
    level: number
    volumeLimit: number
    rate: number
    code: string
    type: string
    createdAt: string
    updatedAt: string
}

declare interface IReportLotterNumberSessionReduxState {
    isFetching?: boolean
    code?: any
    data?: any
    error?: string
}

declare interface IReportDepositWithdrawReduxState {
    isFetching?: boolean
    code?: any
    data?: any
    error?: string
}


declare interface IRequestReportDepositWithdraw {
    date: string
    type: string
}

declare interface IReportDepositWithdraw {
    dataList: {[key: string]: string | number}[]
    balance: number
    graphType: 'horizontal' | 'vertical'
}

declare interface IReportDepositWithdrawList {
    type: string;
    ยอดรวมฝากเงิน?: number 
    ยอดรวมถอนเงิน?: number
}


declare interface IReportState {
    overview: IReportOverviewReduxState
    yegee: IReportYegeeReduxState
    lotterNumberSession: IReportLotterNumberSessionReduxState
    lotterDepositWithdraw: IReportDepositWithdrawReduxState
}
