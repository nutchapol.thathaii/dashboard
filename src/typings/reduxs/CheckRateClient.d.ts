declare interface ICheckRateClientRedux {
    data: any
    isFetching: boolean
    error: string
    code: number
}

declare interface IRateClientState {
    rateClient: ICheckRateClientRedux
}