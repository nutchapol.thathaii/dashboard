declare interface IYegeeConfigState {
    isFetching?: boolean
    data?: IYegeeConfigAll
    error?: string
    code?: number | string
}


declare interface IPutYegeeConfigState {
    round: string
    between_minutes: string
    start_at: string
    expect_rate: string
}

declare interface IYegeeState {
    getYegeeConfig: IYegeeConfigState
    putYegeeConfig: IYegeeConfigState
}