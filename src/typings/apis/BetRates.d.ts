declare interface IBetRatesAll {
    id: number
    type: string
    rate: string
    createdAt: string
    updatedAt: string
}