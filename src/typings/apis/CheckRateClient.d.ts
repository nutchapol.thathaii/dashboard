declare type IRateType = 'lotter'
    | 'game'
    | 'ball'

declare interface IPostCheckRateClient {
    rate: string
    type: IRateType
}

declare interface IPostCheckRateClientResponse {
    rate: string
}