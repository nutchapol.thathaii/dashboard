declare interface ITransactionAll {
  id: number
  money: number,
  status: string,
  userTxTime: string,
  userId: number,
  userBankId: number,
  webBankId: number,
  walletId: number,
  createdAt: string,
  updatedAt: string
}

declare type IWalletTransactionType = 'DEPOSIT' | 'WITHDRAW'
declare type IWalletTransactionStatus = 'SUCCESS' | 'FAIL' | 'WAIT'

declare interface IGetTransactionParams {
  type?: IWalletTransactionType
  status?: IWalletTransactionStatus
  page?: number
  limit?: number
  search?: string
  date?: string
}

declare interface INotification {
  depositTxWaitTotal: number
  withdrawTxWaitTotal: number
  depositTxReserveTotal: number
  withdrawTxReserveTotal: number
}

declare interface ITransactionPutData {
  wallet_tx_id: number
  status: string
  web_bank_id?: number | null
  user_tx_time?: string
  pay_slip_image?: string
  description?: string
}

declare interface IWalletTransactionReserve {
  wallet_tx_id: number
}

declare interface IWalletTransactionCancelReserve {
  wallet_tx_id: number
}

declare interface IWalletTransactionForceCancelReserve {
  wallet_tx_id: number
}
