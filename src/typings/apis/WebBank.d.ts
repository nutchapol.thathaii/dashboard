declare interface IWebBank {
    id: number,
    type: string,
    name: string,
    number: string,
    currentMoney: number,
    status: string
    created_at: string,
    updated_at: string,
}
