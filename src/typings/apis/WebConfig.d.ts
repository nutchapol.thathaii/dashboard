declare interface IWebConfig {
    id: number
    textRunner: string
    contactUrl: string
    contactLine: string
    contactPhoneNumber: string
    updatedAt: string
}