declare type TGameMode = | 'MANUAL' | 'AUTOMATIC'
declare type TGameStatus = | 'OPEN' | 'CLOSE' | 'WAIT' | 'UNKNOWN'

declare interface ILottoSchedule {
    id: number
    code: TLottoSlug
    mode: TGameMode
    status: TGameStatus
    startTime: string
    endTime: string
    updatedAt: string
}

declare interface ILottoResult {
    id: number
    value: string
    value_type: string
    slug: string
    created_at: string
}

declare interface ILottoRateLevel {
    id: number
    level: number
    volumeLimit: number
    rate: string
    code: TLottoSlug
    type: TLottoGameType
    created_at: string
    updated_at: string
}