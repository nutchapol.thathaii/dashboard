declare interface IYegeeConfigAll {
    id: number
    round: string
    betweenMinutes: string
    startAt: string
    createdAt: string
    updatedAt: string
    expectRate: string
}