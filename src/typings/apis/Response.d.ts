declare interface APIResponse<T = any> {
  data: T
  code: number
  devMessage: string
}

declare interface PaginationResponse<T = any> {
  dataList: T[]
  limit: number
  page: number
  total: number
}