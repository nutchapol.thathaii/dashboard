declare interface IManagementPostData {
  username: string
  password: string
  password_confirm: string
  phone_number: string
  permission: string
  bank: IManagementBank
  affilate_uuid: string | null
}

declare interface IManagementBank {
  type: string
  name: string
  number: string
}
declare interface IManagementPutData {
  id: number
  username: string
  password: string
}

declare interface IManagementDeleteData {
  id: number
}