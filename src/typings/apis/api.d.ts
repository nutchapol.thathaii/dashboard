declare interface APIPaginationQuery {
    page?: number
    limit?: number
}

declare interface APIPagination<T = any> {
    page: number
    limit: number
    total: number
    dataList: T[]
  }