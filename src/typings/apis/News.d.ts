declare interface INews {
    id: number
    title: string
    description: string
    createAt: string
    updatedAt: string
    userId: number
}