declare type TCreditFinanceType = | 'FINANCE_DEPOSIT' | 'FINANCE_WITHDRAW'
declare type TCreditLottoType = | 'BET_LOTTER_YEGEE' | 'BET_LOTTER_GOVERNMENT'

declare type TTransactionType = |
  'DEPOSIT'
  | 'WITHDRAW'
  | 'DEPOSIT_GAME_FISHING'
  | 'WITHDRAW_GAME_FISHING'
  | 'BET_GAME_CASINO'
  | 'AFFILATE_WITHDRAW'
  | 'SYSTEM_INCOME'
  | 'SYSTEM_MONEY_LOSS'
  | 'DEPOSIT_SYSTEM_INCOME'
  | 'WITHDRAW_SYSTEM_INCOME'
  | 'WITHDRAW_SYSTEM_MONEY_LOSS'

  declare type TFinanceStatus = | 'WAIT' | 'SUCCESS' | 'FAIL' | 'CANCEL' | 'DRAW'
declare type TBetStatus = | 'WAIT' | 'WINNER' | 'LOSER' | 'DRAW'

declare interface ICreditDetail {
  money: string
  numbers: string
  numbersBetResult: string
  rate: string
  type: TTransactionType | TLottoGameType
  status: TFinanceStatus | TBetStatus
  updatedAt: string
  createDate: string
  slug: string
}

declare interface ICredit {
  groupType: TCreditFinanceType | TCreditLottoType | ''
  createdAt: string
  money: number
  slug: string | null
  status: TFinanceStatus | TBetStatus | ''
  list: ICreditDetail[]
}