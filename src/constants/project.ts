
const project: IProjectConstants = {
  name: 'mantra',
  environmentName: process.env.REACT_APP_MANTRA_ENV || 'dev',
  environment: {
    prod: {
      name: 'production',
      api: 's',
      socket: 's',
    },
    release: {
      name: 'release',
      api: 'https://stapi.tlbetmantra.com/api',
      socket: 'https://stapi.tlbetmantra.com',
    },
    dev: {
      name: 'development',
      api: 'https://stapi.tlbetmantra.com/api',
      socket: 'https://stapi.tlbetmantra.com',
    },
    // dev: {
    //   name: 'development',
    //   api: 'https://devapi.tlbetmantra.com/api',
    //   socket: 'https://devapi.tlbetmantra.com',
    // },
  },
}

export default project