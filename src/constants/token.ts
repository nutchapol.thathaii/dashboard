export const TOKEN = {
    SECRET_KEY: 'TI-Ia!ndI3eTdI3',
    ERROR: {
        JWT_ERROR: 'JsonWebTokenError',
        TOKEN_EXPIRE: 'TokenExpiredError',
        INVALID_SIGNATURE: 'invalid signature',
        JWT_NOT_PROVIDED: 'wt must be provided',
    },
}